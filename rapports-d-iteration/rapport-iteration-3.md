Rapport d'itération n°3 (du 2021-11-15 au 2021-11-21)
==============

*Rappel du trello : []()https://trello.com/b/T0M0iUVY/projet*

## Composition de l'équipe 
Romain De Bodinat | **DEBR17109905** (SCRUM Master)  
Mathias Durand | **DURM13099909**  
Thomas Gouyet | **GOUT01110007**  
Matthieu Pouliquen | **POUM12049908**    
Samuel Souville–Chassaing | **SOUS04129806**  


# Bilan de l'itération précédente  

## Liste des tâches
>* Système de course poursuite : Thomas
>* Animation du player pendant la phase d'enquête : Thomas 
>* Réalisation d'asset 3D : Mathias, Matthieu
>* Réalisation d'asset 2D : Mathias
>* Actions du joueur Enquête : Matthieu
>* Son : Samuel (selon l'avancement des autres travaux)
>* Poursuite des dialogues et du scénario  : Samuel, Romain

## Évènements 

>* Réunion de milieu de sprint jeudi

## Taux de complétion de l'itération  

>* Thomas 110% (plus que prévu)
>* Samuel 80%
>* Matthieu 100%
>* Mathias 90%
>* Romain 80%

# Présentation du travail sur les tâches

## Système de course poursuite (110%) : Thomas

>Implémentation du gameplay de la course poursuite : 
>- déplacement automatique des personnages, 
>- création d'obstacles,
>- animations de course, de dégâts, de défaite et de victoire,
>- textures non définitives posées sur les personnages,
>- musique jouée en fond.

### Difficultés rencontrées :

>- Des difficultés ont été rencontrées quant à la gestion de la physique saut et de la glissade avec un objet 3D (cylindre) et non directement avec les animations (perte de temps car suppression de ce qu'avait été fait à ce niveau).
>- La gestion de l'animation a aussi été dure à appréhender.
>- L'affichage des barres de progression a aussi été compliqué. Une base était présente avec celle de l'UI des dialogue, mais il fallait en plus ici afficher des images symbolisant le joueur et le target (l'ennemi à  rattraper), le problème étant qu'il fallait trouver comment faire pour que les images apparaissent au bonne endroit et quelle était la bonne valeur de x à incrémenter pour que le déplacement du logo concorde avec le déplacement du target.
>- Des difficultés quant à l'animation des dégâts et l'ordre dans lequel ces dernières se déclenchaient se sont aussi manifestées (glissade à la place de saut par exemple)
>- Pour l'animation il a fallu trouver comment bloquer l'animation dans son déplacement et sa rotation en x, y et z. Cependant, comme la rotation des objets dans l'animation était bloqué, il a fallu changer manuellement la hitbox du joueur pendant le saut et la glissade, ce qui a également été un problème

##

## Réalisation d'asset 2D (90%) : Mathias

> A l'exception des tags dont Samuel avait des idées, tous ont été réalisés. Pas de difficultés particulières soulevées.

## Actions du joueur Enquête (100%) : Matthieu

> Quelques mécaniques ont dû être paufinées
>- le joueur se déplace vers un élément avant d'intéragir avec lui
>- lorsqu'il intéragit avec un élément, il ne peut faire d'autre action tant que l'interaction est en cours
>- Un système d'intéraction entre deux item est implémenté
>- Rajout d'un système de porte
>ce qui a été fait sans soucis particulier à relever.

## Réalisation d'assets 3D (5%) : Mathias, Matthieu, Thomas

> Cette tâche a été reportée pour pouvoir correctement finir les autres.

## Son : Samuel

> Cette partie a été mise en suspens pour une prochaine itération, Samuel et Romain ayant préféré s'attarder plus longuement sur l'avancement de la partie level-design et scénario

## Poursuite des dialogues et du scénario (85%) : Samuel, Romain

> Création de croquis de level design ainsi que de premières fiches de personnage.
>- Romain a réalisé un croquis de la zone du Bar ainsi qu'un pour la distillerie. C'est une vue d'en haut, permettant de spacialiser les différents éléments afin de faciliter le travail de modélisation des niveaux qui allait venir dans les prochaines itérations.
>- En parallèle, Samuel a commencé à créer des fiches de personnages avec pour chacun un nom/prénom, un rôle dans l'histoire, et leurs différentes apparitions. Les dialogues associés n'ont pas encore été rédigés.

### Difficultés rencontrées :

> Ce travail a été compliqué notamment dû au fait qu'on partait de rien. Il fallait donc parvenir à rendre tangible les idées jusque-là énoncées et cela a demandé un certain travail de visualisation.
	

# Rétrospective de l'itération précédente
  
## Bilans des retours et des précédentes actions 

>* Les séances communes de travail se sont poursuivies, aidant à la fluidité de l'avancement dans les tâches,
>* maintenant que le projet est bien plus tangible, il est de plus en plus simple de visualiser les prochaines tâches et les interactions entre celles-ci.


## Actions prises pour la prochaine itération

>* Poursuivre le présentiel quand cela est possible,
>* continuer d'organiser des réunions.

# Prévisions de l'itération suivante  

## Évènements prévus  

>* Troisième revue de sprint avec le professeur
>* Plusieurs projets scolaires parallèles

## Titre des tâches reportées  

>* Dialogues (cette tâche restera présente probablement jusqu'à la fin vu son ampleur)

## Titre des nouvelles tâches  

>* Réalisation d'asset 3D
>* Création des niveaux

# Confiance 
## Taux de confiance de l'équipe dans l'itération  

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Groupe 2 	|  *0* 	|  *0* 	    |  *5* 	|  *0* 	|

## Taux de confiance de l'équipe pour la réalisation du projet 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Groupe 2 	|  *0* 	|  *0* 	    |  *4* 	|  *1* 	|

# Illustrations
 
![Croquis du bar](https://cdn.discordapp.com/attachments/900807009924972544/912014002995998720/BarPetit.png)
<div align="center">
Etat actuel du croquis du bar.</div>  
<hr>

![Croquis de la distillerie](https://cdn.discordapp.com/attachments/900807009924972544/912013543363182633/distillerie_1.png)
<div align="center">
Etat actuel du croquis de la distillerie et du repère.</div>  
<hr>

![Présence des personnages](https://cdn.discordapp.com/attachments/900807009924972544/912013571158847518/LieuxPersonnages.png)
<div align="center">
Répartition actuelle des personnages selon les actes et niveaux.</div>  
<hr>

![Titre](https://cdn.discordapp.com/attachments/900807009924972544/912013147966177350/samlllogo.png)
<div align="center">
Asset 2D représentant le titre du jeu.
</div>
<hr>

<div align="center">
<img src=https://cdn.discordapp.com/attachments/900807009924972544/912013304820564088/cursor-interact-pnj-moved.png />

Asset 2D représentant le curseur de discussion.
</div>
<hr>

<div align="center">
<img src=https://cdn.discordapp.com/attachments/900807009924972544/912013421082468422/tmpdesign2.png width=50% height = 50%/>

Asset 2D représentant une peinture de mur.
</div>
<hr>

<div align="center">
<img src=https://cdn.discordapp.com/attachments/900807009924972544/912013490238152734/phonebook-svgrepo-com.png width=50% height = 50%/>

Asset 2D représentant un icône de l'inventaire.
</div>
<hr>

<div align="center">
<img src=https://cdn.discordapp.com/attachments/900807009924972544/912013629191249980/the_artist27s_father_reading__l27evenement__1970.png width=50% height = 50%/>

Asset 2D représentant un tableau.
</div>
<hr>

![](https://cdn.discordapp.com/attachments/900807009924972544/912012275689345084/degats.png)
<div align="center">
Version (antérieure niveau design) de la course-poursuite montrant l'animation actuelle des dégâts : <a href="https://youtu.be/EmHxk4WN7s4">video</a>.
</div>

![](https://cdn.discordapp.com/attachments/900807009924972544/912010976461721740/sprint2.png)
<div align="center">
Seconde version de la navigation par NavMesh par Mathias et Thomas : <a href="https://youtu.be/49mtFo4x2kM">video</a>. Une animation se déclenche lorsqu'on interagit avec les objets et la porte mène à la scène de la course-poursuite.
</div>
