Rapport d'itération n°4 (du 2021-11-22 au 2021-11-28)
==============

*Rappel du trello : []()https://trello.com/b/T0M0iUVY/projet*

## Composition de l'équipe 
Romain De Bodinat | **DEBR17109905**  
Mathias Durand | **DURM13099909**  
Thomas Gouyet | **GOUT01110007** (SCRUM Master)   
Matthieu Pouliquen | **POUM12049908**    
Samuel Souville–Chassaing | **SOUS04129806**  


# Bilan de l'itération précédente  

## Liste des tâches

>* UI Phase d'enquete - Interface inventaire : Thomas
>* Modélisation assets 3D : Thomas
>* Réalisation d'asset 3D : Mathias, Matthieu
>* Réalisation d'asset 2D : Mathias
>* Level Design : Romain, Samuel
>* Poursuite des dialogues et du scénario  : Samuel, Romain
>* Developpement des régions : Mathias

## Évènements 

>* Séance de travail en groupe à L'UQAC mercredi

## Taux de complétion de l'itération  

>* Thomas 80%
>* Samuel 80%
>* Matthieu 100%
>* Mathias 70%
>* Romain 90%

# Présentation du travail sur les tâches

## UI Phase d'enquete - Interface inventaire  : Thomas

> Modélisation basique de l'inventaire avec la gestion :
>- Des objets ramassés 
>- Des indices récupérés
>- Des fiches des personnages rencontrées

Pour l'instant n'a été développé qu'une interface basique, le style et la méthode d'affichage de l'UI de l'inventaire seront développé durant les prochains sprints.

Le système d'image n'est pas encore opérationnel.

### Difficultés rencontrées :

Le système d'items n'était pas encore totalement développé. J'ai dû travailler dessus afin de pouvoir commencer la partie sur les UI.

## Modélisation assets 3D : Thomas

Nous avons utilisé un pack d'asset afin de modéliser nos différents personnages. Cependant, ce pack ne propose que peu de choix quant à la personnalisation de nos personnages.
Nous avons le visuel de notre personnage principal déjà bien établie, j'ai ainsi modifié les éléments déjà présents dans le pack d'assets afin de créer de nouvels éléments afin de pouvoir afficher notre personnage tel que nous le voulons.

### Difficultés rencontrées :

La modélisation avec blender est très lente et complexe à réaliser, cette tache m'a demandé beaucoup de temps pour un résultat pour l'instant insuffisant.
J'ai eu de grosse difficultés à essayer de modéliser divers éléments comme des vêtements ou des barbes.
De plus, j'ai rencontré de nombreux problèmes quant à l'export de mon fichier .FBX, car la configuration initiale de blender faisait disparaître le squelette de mon personnage et j'ai eu du mal à comprendre l'origine de ce problème et à comment le résoudre. 

## Réalisation d'asset 3D : Mathias, Matthieu

Nous avons continué la recherche d'asset pour notre jeu et plusieurs packs intéressants on été trouvés :
>*Industrial Machine Models
>*Low Poly Office 10€
>*Low Poly Office Props - LITE
>*Office Room Furniture
>*Office Supplies Low Poly
>*Toilet(PBR)
>*PBR Game-Ready Desks Pack
>*Wood Barrel
>*Wood Crate
>*Pool Billiards Asset Pack - Devil's Garage

Les différents packs d'assets déjà utilisé (Polygon Sci Fi ...) ont été triés afin de mieux pouvoir être utilisé.

### Difficultés rencontrées :

Il s'agit d'une activité chronophage et souvent les assets intéressants sont payants.

## Réalisation d'asset 2D : Mathias

## Level Design : Romain, Samuel

Romain a continué à développer les plans des régions du jeu :
>* La station de police
>* La place centrale

<br/><br/><br/>

## Poursuite des dialogues et du scénario  : Samuel, Romain

Fin de l'écriture des personnages de l’histoire et de la confection de leur fiche de personnage regroupant des informations utiles au joueur ou à la narration.

Écriture de l’enchaînement des différentes action et quêtes à accomplir dans l’acte 1.


# Rétrospective de l'itération précédente
  
## Bilans des retours et des précédentes actions 

>* Les séances communes de travail ont permis de travailler plus efficacement sur l'élaboration de dialogues et de l'histoire . 

## Actions prises pour la prochaine itération

>* Poursuivre le présentiel quand cela est possible,
>* continuer d'organiser des réunions.

# Prévisions de l'itération suivante  

## Évènements prévus  

>* Quatrième revue de sprint avec le professeur
>* Plusieurs projets scolaires parallèles

## Titre des tâches reportées  

>* Dialogues (cette tâche restera présente probablement jusqu'à la fin vu son ampleur)
>* Modélisation 3D

##Developpement des régions : Mathias

Développement des régions à partir du level design.

## Titre des nouvelles tâches  


# Confiance 
## Taux de confiance de l'équipe dans l'itération  

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Groupe 2 	|  *0* 	|  *0* 	    |  *5* 	|  *0* 	|

## Taux de confiance de l'équipe pour la réalisation du projet 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Groupe 2 	|  *0* 	|  *0* 	    |  *4* 	|  *1* 	|

# Illustrations

 ![Croquis du poste de police](https://cdn.discordapp.com/attachments/909329477941747762/914347067156078632/Police.png)
<div align="center">
Etat actuel du du poste de police.</div>  
<hr>

 ![Croquis de la place centrale](https://cdn.discordapp.com/attachments/909329477941747762/914347067420332032/Place.png)
<div align="center">
Etat actuel de la place centrale.</div>  
<hr>

<div align="center">
<img src=https://cdn.discordapp.com/attachments/909329477941747762/914330699929104454/unknown.png width=50% height = 50%/><br/>
Image du bar.</div>  
<hr>

<div align="center">
<img src=https://cdn.discordapp.com/attachments/909329477941747762/914330943924342814/unknown.png width=50% height = 50%/><br/>
Aperçu de la distillerie.</div>  
<hr>

<div align="center">
<img src=https://cdn.discordapp.com/attachments/909329477941747762/914364603784384522/unknown.png width=50% height = 50%/><br/>
Illustration du poste de police.</div>  
<hr>

<div align="center">
<img src=https://cdn.discordapp.com/attachments/909329477941747762/914349201931005982/unknown.png width=50% height = 50%/><br/>
Exemple de personnage.</div>  
<hr>

<div align="center">
<img src=https://cdn.discordapp.com/attachments/909329477941747762/914349201931005982/unknown.png width=50% height = 50%/><br/>
Exemple de personnage.</div>  
<hr>

<div align="center">
<img src=https://cdn.discordapp.com/attachments/909329477941747762/914350333180903494/unknown.png width=50% height = 50%/>
Exemple de personnage.</div>  
<hr>

<div align="center">
<img src=https://cdn.discordapp.com/attachments/909329477941747762/914352068788097044/unknown.png width=50% height = 50%/><br/>
Exemple de personnage.</div>  
<hr>

<div align="center">
<img src=https://cdn.discordapp.com/attachments/765665001562177546/914366337336352848/unknown.png width=50% height = 50%/>
<br/>
Exemple affichage inventaire en vidéo : <a href=https://cdn.discordapp.com/attachments/900807009924972544/914353524974972988/Enregistrement_1.mp4>lien</a>.</div>  
<hr>




