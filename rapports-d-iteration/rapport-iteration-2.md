# Rapport d'itération n°2 (du 2021-11-08 au 2021-11-14)

*Rappel du trello : []()https://trello.com/b/T0M0iUVY/projet*

## Composition de l'équipe 
Romain De Bodinat | **DEBR17109905**  
Mathias Durand | **DURM13099909**  
Thomas Gouyet | **GOUT01110007**  
Matthieu Pouliquen | **POUM12049908** (SCRUM Master)   
Samuel Souville–Chassaing | **SOUS04129806**  


## Bilan de l'itération précédente  

### Liste des tâches
>* Interaction à la souris : Romain, Matthieu
>* Actions du joueur Enquête : Mathias, Matthieu
>* Actions du joueur Interrogation : Thomas
>* Mise en place de l'UI des dialogues : Thomas
>* Dialogues : Samuel
>* Scénarios : Samuel

### Évènements 

>* Réunion de milieu de sprint jeudi

### Taux de complétion de l'itération  

>* Thomas 100%
>* Samuel 60%
>* Matthieu 80%
>* Mathias 80%
>* Romain 90%

### Présentation du travail sur les tâches

>* Interaction à la souris (100%) : Romain, Matthieu
	Détecter les éléments sous le cuseur de la souris et le modifier selon les conditions
>* Actions du joueur Enquête (60%) : 
	Utilisation du navmesh pour déplacer le personnage
	Ajout des intéractions avec le curseur (personnages, indices, objets)
>* Actions du joueur Interrogation (100%) : Thomas
	Mise en place d'un timer lié au dialogue et création d'une interface de base
>* UI Dialogue (100 %) : Thomas
	Mise en place de l'UI des dialogues
>* Dialogues : Samuel
	élaboration des fiches personnages, intégrés à l'enquête (90%)
>* Scénarios : Samuel
	élaboration et peaufinement du dialogue
>* Sons : Samuel
	composition du motif musical et du thème principal
	

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 

>* Plus de séance de travail en commun et une réunion pour mieux se coordonner
>* les différentes scenes ont permi d'éviter les conflit de version
>* la bonne entente nous a permi de mieux travailler

### Actions prises pour la prochaine itération

>* Favoriser le présentiel pour être le plus productif
>* Continuer d'organiser des réunions

## Prévisions de l'itération suivante  
### Évènements prévus  

>* Deuxième revue de sprint avec le professeur
>* Plusieurs projets scolaires parallèles
>* Division de la tache dialogue en plusieurs sous taches en vu de sa complexité

### Titre des tâches reportées  

>* Son
>* Actions du joueur Interrogation (qui était prévu dans l'organisation)


### Titre des nouvelles tâches  
>* Système de course poursuite
>* Réalisation d'asset 3D
>* Réalisation d'asset 2D

## Confiance 
### Taux de confiance de l'équipe dans l'itération  

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Groupe 2 	|  *0* 	|  *0* 	    |  *5* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Groupe 2 	|  *0* 	|  *0* 	    |  *2* 	|  *3* 	|

## Illustrations
 
![Capture d'écran de Ableton Live 10](https://cdn.discordapp.com/attachments/900807009924972544/909259378002104360/unknown.png)
<div align="center">
Capture d'écran du logiciel (<i>Ableton Live 10</i>) ayant permis à Samuel de réaliser des sons et musiques
</div>  
<hr>

![UI de dialogue](https://cdn.discordapp.com/attachments/900807009924972544/909294980936380426/ImageDialogueUI.PNG)
<div align="center">
Première version de l'UI de dialogue par Matthieu et Thomas. Voir le <a href="https://cdn.discordapp.com/attachments/900807009924972544/909296341988028456/Parcour1.mp4">parcours 1</a> et le <a href="https://cdn.discordapp.com/attachments/900807009924972544/909296371226538064/Parcour2.mp4">parcours 2</a> et <a href="https://cdn.discordapp.com/attachments/900807009924972544/909310472837275708/Parcours3.mp4">parcours 3</a> en vidéo.
</div>
<hr>

![Interraction objets](https://cdn.discordapp.com/attachments/765665001562177546/909298009064497183/unknown.png)
<div align="center">
Interractions avec les objets par Romain et Matthieu. Sur la <a href="https://cdn.discordapp.com/attachments/900807009924972544/909108892317933630/Enregistrement_6.mp4">vidéo</a>, on voit un exemple avec l'ouverture d'une boite de dialogue (version de test). Les autres interractions sont présentées dans une autre <a href="https://cdn.discordapp.com/attachments/783465861134155808/909317866170486784/Interaction_souris.mp4">video</a>.
</div>
<hr>

![](https://cdn.discordapp.com/attachments/765665001562177546/909308144176496680/unknown.png)
<div align="center">
Première version de la navigation par NavMesh par Mathias : <a href="https://youtu.be/B7hjRiWRpUE">video</a>. Une version intégrée avec les autres ajouts du groupe est prévue pour la fin du sprint, lundi 16 novembre.
</div>
