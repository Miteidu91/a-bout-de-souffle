# Rapport d'itération n°1 (du 2021-11-01 au 2021-11-08)

*Rappel du trello : []()https://trello.com/b/T0M0iUVY/projet*

## Composition de l'équipe 
Romain De Bodinat | **DEBR17109905**  
Mathias Durand | **DURM13099909** (SCRUM Master)  
Thomas Gouyet | **GOUT01110007**  
Matthieu Pouliquen | **POUM12049908**  
Samuel Souville–Chassaing | **SOUS04129806**  


## Bilan de l'itération précédente  
### Évènements 

>* Difficultés à trouver ce qu'il fallait faire
>* Difficultés à trouver les assets
>* Difficultés d'emploi du temps avec les autres rendus de projet
>* Comprendre comment mettre en place les interraction
>* Problèmes de codes mais résolus
>* Manque d'un petit peu de temps pour une tâche (inventaire) --> manque UI

### Taux de complétion de l'itération  

>  2 terminés / 6 prévues = 33%


### Tâches terminées

> * Système d'arbres de dialogue (par Matthieu) -->  Arbre pleinement fonctionnel. on peut définir des prédécesseurs et successeurs pour les dialogues (dans un fichier texte), ainsi que des conditions de débloquage. "*Un dialogue est un graphe de ligne de réplique*".   
> Quand il y a plusieurs réplique à un nœud, il s'agit d'un choix pour le joueur, qu'il peut sélectionner (dans la scène "Matthieu") avec les boutons 1, 2 ou 3.  
> Dans la scène de test actuelle, les dialogues se lancent automatiquement dans la console et il est possible de faire des choix avec le clavier.  
>
> * Système d'inventaire / de collection d'objets/indices : manque les tests avec l'UI (sous tâches de "UI") (Mathias) -->  
> Les scripts sont tous créés, et compilent sans erreurs mais n'ont pas été testés en conditions réelles : manque l'UI qui fait usage de l'inventaire, à définir dans la partie "UI". Cette tâche est à revoir
> * \[Son] (Samuel) : trouver les outils qui correspondent à l'époque et à l'identité musicale qu'on cherche à avoir : 90%
> * \[Scénario] (Samuel) : trame principale : 100%

### Présentation du travail sur les autres tâches

> * Assets 3D (Thomas) --> package d'assets sur le DIM : à trier parmis l'environnement global fourni. Propose de nombreux assets dits "low-poly". La tâche est mise en pause pour se concentrer sur d'autres tâches
>
> * Interractions à la souris (Romain) --> tests basiques et tutoriels ont été suivis

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 

>  Nous avons pas tous été assez constant et efficaces sur le travail.
>
> La bonne entente des membres de l'équipe est un moteur pour l'ensemble des membres et permet l'emergence d'idées assez facilement
>
> Aucun meeting n'a été mis en place pendant le sprint
>
> Essayer de se libérer des créneaux de travail
### Actions prises pour la prochaine itération

 > Faire de nouvelles scènes sur le git
### Axes d'améliorations 

> Meetings pour faire un compte rendu de l'avancement du sprint et s'informer sur l'avancement de chacun, mettre en lumière des problèmes rencontrés en avance. Nous allons en faire tous les 3 jours environ sur les prochains sprints
>
> Réorganiser le git pour plus de clarté et un travail plus facile. Idée : une scène par fonctionnalité ou par personne.

## Prévisions de l'itération suivante  
### Évènements prévus  

> Une revue de sprint est prévue avec le professeur à l'issue du prochain sprint

### Titre des tâches reportées  

> * Scénario + Dialogues + création personnages (10%)
> * Ineraction à la souris (15%) : à finir coder
> * Assets 3D (50%) : certains à faire (personnages notamment), les autres à choisir --> peuvent être reportés sans conséquences car dépendent du scénario en cours de développement
> * Système d'inventaire (90%) : UI de test à créer pour vérifier la totalité
### Titre des nouvelles tâches  
> * Dialogues
> * Action du joueur Enquête
> * Action du joeuur Interrogation
> * IA (pour le navmesh et des recherches sur la course poursuite)
## Confiance 
### Taux de confiance de l'équipe dans l'itération  

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Groupe 2 	|  *0* 	|  *4* 	    |  *1* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Groupe 2 	|  *0* 	|  *0* 	    |  *5* 	|  *2* 	|


## Illustrations

Peu d'éléments graphiques ont été développés cette semaine, beaucoup de code et de réflexions

![Pack d'assets](https://cdn.discordapp.com/attachments/900807009924972544/906593352248619108/unknown.png)
<div align="center">
Pack d'assets trouvé par Thomas duquel nous récupèrerons probablement quelques éléments en temps venu.
</div>
<hr>

![Console de dialogue](https://cdn.discordapp.com/attachments/900807009924972544/908942318793531462/unknown.png)
<div align="center">
Première version de l'arbre de dialogue en ligne de commandes par Matthieu.
</div>
<hr>

![Extrait code](https://cdn.discordapp.com/attachments/765665001562177546/909296396048404510/unknown.png)
<div align="center">
Extrait de code pour les Items en jeu par Mathias.
</div>
