using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum mode { fixe, follow, run }
public enum direction{NE,SE,SW,NW}
public class CameraController2 : MonoBehaviour
{
    public GameObject player;
    private mode mode = mode.fixe;
    public direction direction;
    private direction newDirection;
    private GameObject toFollow;
    private float camSizeTarget = 8;
    private Camera cam;
    private float minSizeCam = 4;
    private float maxSizeCam = 16;
    private Bounds bound;


    public static CameraController2 Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        cam = transform.GetComponentInChildren<Camera>();
        float targetRot = (45 + 90 * (int)direction) % 360;
        gameObject.transform.rotation = Quaternion.Euler(0, targetRot, 0);
        newDirection = direction;

        FollowPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        UpdateLogic();
        SetOutput();
    }

    private void GetInput()
    {
        if(mode==mode.fixe || mode == mode.follow)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                newDirection = (direction)((4 + (int)(direction + 1)) % 4);
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                newDirection = (direction)((4 + (int)(direction - 1)) % 4);
            }
        }
        if ( mode == mode.follow)
        {
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                camSizeTarget -= Input.GetAxis("Mouse ScrollWheel") * 10;
                camSizeTarget = Mathf.Clamp(camSizeTarget, minSizeCam, maxSizeCam);
            }
        }
    }
    private void UpdateLogic()
    {

    }
    private void SetOutput()
    {
        if (direction != newDirection)
        {
            StopAllCoroutines();
            StartCoroutine(rotate());
        }
        if (mode == mode.follow)
        {
            gameObject.transform.position += (toFollow.transform.position - gameObject.transform.position) / 10;
        }
        if (mode == mode.fixe)
        {
            gameObject.transform.position += (bound.center - gameObject.transform.position) / 10;
        }

        if(Mathf.Abs(cam.orthographicSize - camSizeTarget) > 0.005f)
        {
            cam.orthographicSize += (camSizeTarget - cam.orthographicSize) / 10;
        }
        else
        {
            cam.orthographicSize = camSizeTarget;
        }
    }

    private IEnumerator rotate()
    {
        float curRot = gameObject.transform.rotation.eulerAngles.y;
        float targetRot = (45 + 90 * (int)newDirection) % 360;
        float transition;

        direction = newDirection;
        for (float i = 1; i > 0; i -= Time.deltaTime)
        {
            if (Mathf.Abs(curRot - targetRot) > 180)
            {
                float auxCurRot = (curRot + 180) % 360;
                float auxTargetRot = (targetRot + 180) % 360;
                transition = (auxTargetRot + (i / 1) * (auxCurRot - auxTargetRot) + 180) % 360;
            }
            else
            {
                transition = targetRot + (i / 1) * (curRot - targetRot);
            }
            gameObject.transform.rotation = Quaternion.Euler(0,transition,0);
            yield return null;
        }
        gameObject.transform.rotation = Quaternion.Euler(0,targetRot,0);
        yield return null;
    }
    
    public void Follow(GameObject g)
    {
        mode = mode.follow;
        toFollow = g;
    }
    public void FollowPlayer()
    {
        if (player != null)
        {
            Follow(player);
        }
    }
    public void Fixe(List<GameObject> Lg)
    {
        mode = mode.fixe;
        toFollow = null;
        Bounds b = new Bounds(Lg[0].transform.position, Vector3.zero);
        foreach (GameObject g in Lg)
        {
            b.Encapsulate(g.GetComponent<Renderer>().bounds.max);
            b.Encapsulate(g.GetComponent<Renderer>().bounds.min);
        }
        bound = b;
        ZoomInOnFixeMode();
    }

    private void ZoomInOnFixeMode()
    {
        float l = bound.size.x;
        float L = bound.size.z;
        float d = Mathf.Sqrt(l * l + L * L);
        float alpha = Mathf.Atan(l/L);
        float width = d * Mathf.Sin(alpha + (Mathf.PI / 180) * 45.0f);
        //Debug.Log(bound.size);
        camSizeTarget = (width/cam.aspect)/2 + 1;
        //Debug.Log(width);
        //Debug.Log(cam.aspect);
        //Debug.Log(camSizeTarget);
        camSizeTarget = Mathf.Clamp(camSizeTarget, minSizeCam, maxSizeCam);
        //Debug.Log(camSizeTarget);
    }
}
