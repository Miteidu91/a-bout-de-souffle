using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using UnityStandardAssets.Characters.ThirdPerson;
public class CursorManager : MonoBehaviour
{
    private Vector3 mousePosition = Vector3.zero;
    private RaycastHit hit;
    private Item itemToInteract;
    private bool LeftClic = false;
    private bool RightClic = false;
    public bool CanInteract = true;
    private bool DoInteraction = false;
    //public bool CanMoove = true;
    //public Camera camera = ca;
    public Texture2D Defaultcursor;
    public Texture2D Dialogcursor;
    public Texture2D Actioncursor;
    public Texture2D Interactioncursor;
    private Texture2D Currentcursor;
    private Texture2D Nextcursor;
    private IEnumerator routineDoAction = null;
    //public IEnumerator routineInteract = null;
    public static CursorManager Instance;

    public NavMeshAgent agent;

    public ThirdPersonCharacter character;
    public Animator animator;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (agent)
        {
            agent.updateRotation = false;
            UpdateCursorSprite(Defaultcursor);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (agent )
        {
            GetInput();
            LogicUpdate();
            SetOutput();
        }     
    }

    public void GetInput()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        LeftClic = Input.GetKeyDown(KeyCode.Mouse0);
        RightClic = Input.GetKeyDown(KeyCode.Mouse1);
        DoInteraction = (GameManager.Instance.ToCombineWith != null);
    }
    public void LogicUpdate()
    {
        //r�cup�ration des �l�ments pertinents
        Physics.Raycast(mousePosition, Camera.main.transform.forward, out hit, 50);
        if (hit.transform != null)
        {
            if (hit.transform.TryGetComponent<FichePersonnageGO>(out FichePersonnageGO personGO))
            {
                itemToInteract = personGO.FP;
            }
            else if (hit.transform.TryGetComponent<IndiceGO>(out IndiceGO indiceGO))
            {
                itemToInteract = indiceGO.indice;
            }
            else if (hit.transform.TryGetComponent<ObjectGO>(out ObjectGO objectGO))
            {
                itemToInteract = objectGO.obj;
            }
            else if (hit.transform.TryGetComponent<DoorGO>(out DoorGO doorGO))
            {
                itemToInteract = doorGO.door;
            }
            else if (hit.transform.TryGetComponent<Inventory>(out Inventory inventory))
            {
                //itemToInteract = doorGO.door;
                Debug.Log("Tezst");
            }
            else
            {
                itemToInteract = null;
            }
        }
        else
        {
            itemToInteract = null;
        }


        //MaJ du sprite du cursor
        if (itemToInteract != null)
        {
            if (DoInteraction)
            {
                Nextcursor = Interactioncursor;
            }
            else
            {
                if (itemToInteract is FichePersonnage)
                {
                    Nextcursor = Dialogcursor;
                }
                else
                {
                    Nextcursor = Actioncursor;
                }
            }
        }
        else
        {
            Nextcursor = Defaultcursor;
        }
    }
    public void SetOutput()
    {
        //action a effectuer
        if ((LeftClic||RightClic) && routineDoAction != null)
        {
            StopCoroutine(routineDoAction);
            routineDoAction = null;
        }
        if (LeftClic)
        {
            if(itemToInteract!=null || (hit.transform != null && hit.transform.tag == "Ground"))
            {
                routineDoAction = DoActionAt(hit.point, itemToInteract);
                StartCoroutine(routineDoAction);
            }
        }
        if (RightClic)
        {
            if (hit.transform != null)
            {
                routineDoAction = DoActionAt(hit.point, null);
                StartCoroutine(routineDoAction);
            }
        }
        UpdateCursorSprite(Nextcursor);
    }



    private void UpdateCursorSprite(Texture2D texture)
    {
        if(Currentcursor != texture)
        {
            Currentcursor = texture;
            Vector2 cursorHotspot = new Vector2(texture.width / 2, texture.height / 2);
            if(texture == Dialogcursor)
            {
                cursorHotspot = new Vector2(0.0f, texture.height / 2); ;
            }
            Cursor.SetCursor(Currentcursor, cursorHotspot, CursorMode.ForceSoftware);
        }
    }

    private bool CanReachPosition(Vector3 position)
    {
        NavMeshPath path = new NavMeshPath();
        agent.CalculatePath(position, path);
        return path.status == NavMeshPathStatus.PathComplete;
    }

    private IEnumerator DoActionAt(Vector3 destination, Item interactWith)
    {
        //animator.SetTrigger("Test");
        //Debug.Log("Starting doactionat " + destination);
        //Debug.Log("Old position " + agent.transform.position); 
        if (CanInteract)
        {
            agent.SetDestination(destination);
            animator.SetBool("Mooving", true);
            bool b1 = !agent.pathPending;
            bool b2 = agent.remainingDistance <= agent.stoppingDistance;
            bool b3 = !agent.hasPath || agent.velocity.sqrMagnitude == 0f;
            //while ((agent.pathEndPosition - destination).magnitude > 0.1f || agent.remainingDistance > 0.1f)
            while (!(b1 && b2 && b3))
            {
                b1 = !agent.pathPending;
                b2 = agent.remainingDistance <= agent.stoppingDistance;
                b3 = !agent.hasPath || agent.velocity.sqrMagnitude == 0f;
                if (agent.remainingDistance > agent.stoppingDistance)
                {
                    character.Move(agent.desiredVelocity, false, false);
                }
                else
                {
                    character.Move(Vector3.zero, false, false);
                }
                yield return null;
            }
            
            animator.SetBool("Mooving", false);

            if (interactWith != null)
            {
                if ((character.transform.position - destination).magnitude < 2)
                {
                    if (DoInteraction)
                    {
                        GameManager.Instance.Combine(interactWith);
                    }
                    else
                    {
                        //routineInteract = interactWith.Interact();
                        //StartCoroutine(routineInteract);
                        yield return Animation(interactWith.GetItemType());
                        CanInteract = false;
                        StartCoroutine(interactWith.Interact());
                    }
                }
                    
            }
        }
        //Debug.Log("ending doactionat " + destination);
    }

    private IEnumerator Animation(ItemType itemType)
    {
        switch (itemType)
        {
            case ItemType.Objet :
                animator.SetTrigger("Picking");
                yield return new WaitForSeconds(2);
                break;
            case ItemType.Door:
                animator.SetTrigger("OpenDoor");
                yield return new WaitForSeconds(3);
                break;
            case ItemType.Indice:
                animator.SetTrigger("KneelingInspect");
                yield return new WaitForSeconds(3);
                break;
        }
        yield return null;
    }

    public void StopMooving()
    {
        StopCoroutine(routineDoAction);
        routineDoAction = null;
        animator.SetBool("Mooving", false);
        agent.SetDestination(character.transform.position);
        character.Move(Vector3.zero, false, false);
    }
}
