using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public List<string> LineObtained = new List<string>();
    public List<string> ObjectObtained = new List<string>();
    public List<string> ClueObtained = new List<string>();
    public Item ToCombineWith=null;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Combine(Item it)
    {        
        if(!InventaireUIManager.Instance.itemInteract.gameObject.activeSelf)
        {
            InventaireUIManager.Instance.itemInteract.gameObject.SetActive(true);
            InventaireUIManager.Instance.itemInteract.sprite = it.GetSprite();
        }
        if (ToCombineWith == null)
        {
            ToCombineWith = it;
        }
        else
        {
            InteractionManager.Instance.Interact(it, ToCombineWith);
            ToCombineWith = null;
            InventaireUIManager.Instance.itemInteract.gameObject.SetActive(false);
            InventaireUIManager.Instance.itemInteract.sprite = null;
        }
    }

}
