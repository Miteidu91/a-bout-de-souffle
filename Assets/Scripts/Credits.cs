using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Credits : MonoBehaviour
{
    [SerializeField] Transform bgToMove;
    [SerializeField] Camera mCamera;
    [SerializeField] Transform endOfCredits;
    private bool end = false;

    private void Start()
    {

    }
    void Update()
    {
        bgToMove.Translate(new Vector3(0.0f, 100.0f * Time.deltaTime, 0.0f));
        Vector3 screenPoint = mCamera.WorldToViewportPoint(endOfCredits.transform.position);
        if (!end && screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 70 && screenPoint.y > 0 && screenPoint.y < 1)
        {
            end = true;
            StartCoroutine(EndOfCredits());
        }
    }

    private IEnumerator EndOfCredits()
    {
        yield return new WaitForSeconds(10.0f);
        SceneManager.LoadScene(0);
    }

    public void passerCredits()
    {
        SceneManager.LoadScene(0);
    }
}
