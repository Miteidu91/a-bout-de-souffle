using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CourseManager : MonoBehaviour
{

    public static CourseManager Instance;
    public GameObject target;
    private bool actif = true;
    private bool loose = false;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public bool getGameActif()
    {
        return actif;
    }

    public bool getLoose()
    {
        return loose;
    }

    public void setGameActif(bool a)
    {
        actif = a;
    }

    public void PlayerLoose()
    {
        loose = true;
        setGameActif(false);
        StartCoroutine(GameLoose());
    }
    private IEnumerator GameLoose()
    {
        yield return new WaitForSeconds(3f);
        target.SetActive(true);
        loose = false;
        setGameActif(true);
        SceneManager.LoadScene(3);
    }

    public void PlayerVictory()
    {
        setGameActif(false);
        StartCoroutine(GameWin());
    }

    private IEnumerator GameWin()
    {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(4);
    }
}
