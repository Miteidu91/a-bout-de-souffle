using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class PlayerControler : MonoBehaviour
{
    public float minPlayerSpeed = 0.01f;
    public float maxPlayerSpeed = 0.025f;
    public float glissadeSpeed = 0.020f;
    public float timeMaxSlow = 2f;
    public float timeJumpStartAnim = 0.2f;
    public float glissadeAnimation = 0.8f;
    public float jumpAnimation = 0.35f;
    public float sizeMap;
    public float sizeBarreProgression;
    public Image barreProgression;
    public Image logoPlayer;
    public Animator animator;
    public BoxCollider bCollider;
    public GameObject ImageWin;

    private Rigidbody rb;
    private float playerSpeed;
    private Vector3 scaleChange = new Vector3(0, 0.5f, 0);
    private float glissadeTimer;
    private float jumpTimer;
    private float slowTimer;
    private float jumpStartAnim;
    private bool glisse;
    private bool IsJumping;
    private bool IsSlow;
    private bool endGame = false;
    private bool startJumping = false;
    private float progression = 0f;
    private float offsetBarreProgression;
    private Vector3 centerCollider;
    private Vector3 sizeCollider;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        glissadeTimer = 0.0f;
        jumpTimer = 0.0f;
        slowTimer = 0.0f;
        glisse = false;
        IsJumping = false;
        IsSlow = false;
        playerSpeed = maxPlayerSpeed;
        offsetBarreProgression = logoPlayer.transform.position.x;
        sizeCollider = new Vector3(bCollider.size.x, bCollider.size.y, bCollider.size.z);
        centerCollider = new Vector3(bCollider.center.x, bCollider.center.y, bCollider.center.z);
        animator.SetBool("Player", true);
    }

    // Update is called once per frame
    void Update()
    {
        glissadeTimer -= Time.deltaTime;
        jumpTimer -= Time.deltaTime;
        slowTimer -= Time.deltaTime;
        jumpStartAnim -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Mouse0) && !IsJumping && !glisse)
        {
            Jump();
        }
        if (Input.GetKeyDown(KeyCode.Mouse1) && !IsJumping && !glisse)
        {
            Glisse();
        }

        if (glissadeTimer <= 0 && glisse)
        {
            glisse = false;
            bCollider.size = sizeCollider;
            bCollider.center = centerCollider;
        }

        if (jumpStartAnim <= 0 && startJumping)
        {
            startJumping = false;
            IsJumping = true;
            jumpTimer = jumpAnimation;
            bCollider.center = new Vector3(centerCollider.x, centerCollider.y + 1, centerCollider.z);
        }

        if (jumpTimer <= 0 && IsJumping)
        {
            IsJumping = false;
            bCollider.center = centerCollider;
        }

        

            if (slowTimer <= 0 && IsSlow)
        {
            playerSpeed = maxPlayerSpeed;
            IsSlow = false;
        }

    }

    private void FixedUpdate()
    {
        if (CourseManager.Instance.getGameActif())
        {
            if (!glisse)
            {
                Moove(playerSpeed);
            }
            else
            {
                Moove(glissadeSpeed);
            }
        }
        else if (CourseManager.Instance.getLoose() && !endGame)
        {
            animator.SetTrigger("Loose");
            endGame = true;
        }
        ShowBarreProgression();
    }

    private void ShowBarreProgression()
    {
        progression = transform.position.z / sizeMap;
        logoPlayer.transform.position = new Vector3(offsetBarreProgression + progression * sizeBarreProgression, logoPlayer.transform.position.y, logoPlayer.transform.position.z);
        barreProgression.fillAmount = 1f - progression;
    }
    private void Moove(float playerSpeed)
    {
        transform.Translate(Vector3.forward * playerSpeed);
        return;
    }
    private void Jump()
    {
        startJumping = true;
        jumpStartAnim = timeJumpStartAnim;
        animator.SetTrigger("Jump");
    }

    private void Glisse()
    {
        glissadeTimer = glissadeAnimation;
        glisse = true;
        animator.SetTrigger("Slide");
        bCollider.size = new Vector3(sizeCollider.x, sizeCollider.y - 1f, sizeCollider.z);
        bCollider.center = new Vector3(bCollider.center.x, bCollider.center.y - 0.6f, bCollider.center.z);     
    }

    private void SlowPlayer()
    {
        slowTimer = timeMaxSlow;
        IsSlow = true;
        playerSpeed = minPlayerSpeed;
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("GroundObstacle"))
        {
            SlowPlayer();
            animator.SetTrigger("FailJump");
        }
        if (other.gameObject.CompareTag("TopObstacle"))
        {
            SlowPlayer();
            animator.SetTrigger("FailSlide");
        }
        if (other.gameObject.CompareTag("Target"))
        {
            animator.SetTrigger("Win");
            CourseManager.Instance.PlayerVictory();
        }
    }

}
