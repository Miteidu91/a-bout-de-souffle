using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TargetControler : MonoBehaviour
{
    public float glissadeSpeed = 0.020f;
    public float targetSpeed = 0.035f;
    public float jumpHeight = 3;
    public float fallOffset = 0.03f;
    public float animationTimer = 0.8f;
    public Image logoTarget;
    public float sizeMap;
    public float sizeBarreProgression;
    public float avanceTarget;
    public Animator animator;


    private Rigidbody rb;
    private Vector3 scaleChange = new Vector3(0, 0.5f, 0);
    private bool glisse = false;
    private float glissadeTimer = 0f;
    private float progression = 0f;
    private float offsetBarreProgression;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        offsetBarreProgression = logoTarget.transform.position.x;
        logoTarget.transform.position = new Vector3(offsetBarreProgression + progression * sizeBarreProgression, logoTarget.transform.position.y, logoTarget.transform.position.z);
        animator.SetBool("Player", false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (CourseManager.Instance.getGameActif())
        {
            glissadeTimer -= Time.deltaTime;
            if (!glisse)
            {
                Moove(targetSpeed);
            }
            else
            {
                Moove(glissadeSpeed);
            }


            if (glissadeTimer <= 0 && glisse)
            {
                glisse = false;
            }
        }
        MooveLogoTarget();
    }

    private void MooveLogoTarget()
    {
        progression = transform.position.z / sizeMap;
        logoTarget.transform.position = new Vector3(sizeBarreProgression * progression + offsetBarreProgression, logoTarget.transform.position.y, logoTarget.transform.position.z);
    }

    private void Moove(float playerSpeed)
    {
        transform.Translate(Vector3.forward * playerSpeed);
        return;
    }
    private void Jump()
    {
        animator.SetTrigger("Jump");
    }

    private void Glisse()
    {
        animator.SetTrigger("Slide");
    }

    private void GetUp()
    {
        transform.localScale += scaleChange;
        transform.position = new Vector3(transform.position.x, transform.position.y + scaleChange.y, transform.position.z);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("GroundObstacle"))
        {
            Jump();
        }
        if (other.gameObject.CompareTag("TopObstacle"))
        {
            glissadeTimer = animationTimer;
            glisse = true;
            Glisse();
        }
        if (other.gameObject.CompareTag("Player"))
        {
            animator.SetTrigger("Win");
        }
        if (other.gameObject.CompareTag("EndCourse"))
        {
            gameObject.SetActive(false);
            CourseManager.Instance.PlayerLoose();
            
        }
    }
}
