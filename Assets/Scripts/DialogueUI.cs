using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using UnityEngine.UI;
using System.IO;

[System.Serializable]
public class DialogueUI 
{
    //private string directoryPath = "Assets\\StreamingAssets\\Dialogues";
    //Variable qui contient tout le UI d'un dialogue
    public  GameObject dialogueUI;
    //Variable qui contient la bulle de dialogue
    public  GameObject bulleDialogue;
    //Variable qui contient le texte d'un dialogue
    public  Text textDialogue;
    //Variable qui contient le nom du personnage qui parle 
    public  Text nomPerso;
    public RawImage logoDialogue;
    public Texture m_Texture;
    //Variable qui contient l'ensemble des images des r�ponses
    public  GameObject[] bullesResponses;
    //Variable qui contient le texte des options de reponses
    public  Text[] responsesTexts;
    //Variable qui contient le texte du nom des options des reponses
    public  Text[] optionsNumero;
    public GameObject BarreCountdown;
    public Image ImageBarreCountdown;

    public static DialogueUI Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void visibileDialogue(bool b)
    {
        dialogueUI.SetActive(b);
        return;
    }
    public void visibleBarreCountdown(bool b)
    {
        BarreCountdown.SetActive(b);
        return;
    }

    public void showDialogue(Line line)
    {
        textDialogue.text = line.Text;
        nomPerso.text = line.CharacterId;

        //string directoryPath = "Assets\\Sprites\\Personnage";

        //string path = Path.Combine("Personnage", line.CharacterId);
       string path = "Personnage/" + line.CharacterId;
        //path="Assets/Sprites/Personnage/Danniel Venegas.png";
        Debug.Log(path);
        //logoDialogue.texture = (Texture2D)AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D)); "Personnage/Danniel Venegas"
        logoDialogue.texture = Resources.Load<Texture2D>(path);
        //Debug.Log(path);
        //Texture a = (Texture)Resources.LoadAssetAtPath(path);
    }
    public void showResponses(Dialogue D)
    {
        string s = "";
        for (int i = 0; i < D.CurrentLines.Count; i++)
        {
            bullesResponses[i].SetActive(true);
            s += i.ToString() + " : " + D.CurrentLines[i].Text + " | ";
            optionsNumero[i].text = i.ToString();
            responsesTexts[i].text = D.CurrentLines[i].Text;

        }
    }
    public void showResponses(List<Line> lines)
    {
        string s = "";
        for (int i = 0; i < lines.Count; i++)
        {
            bullesResponses[i].SetActive(true);
            s += i.ToString() + " : " + lines[i].Text + " | ";
            optionsNumero[i].text = i.ToString();
            responsesTexts[i].text = lines[i].Text;

        }
    }
    public void CloseResponses()
    {
        for (int i = 0; i < bullesResponses.Length; i++)
        {
            bullesResponses[i].SetActive(false);
        }
    }

    private string NomPerso(string nom)
    {
        for (int i = 0; i < nom.Length; i++)
        {
            if (Char.IsUpper(nom[i])){
                //nom.add
            }
                
        }
        return nom;
    }









    /*public DialogueUI()
    {

    }*/

    // Start is called before the first frame update
    /*void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }*/


}
