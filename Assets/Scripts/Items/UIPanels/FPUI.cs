using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FPUI : MonoBehaviour
{
    public static FPUI Instance;
    public string title = "";
    public string description = "";

    public Sprite illustration;

    private TextMeshProUGUI titleText;
    //TODO: rajouter illustration
    private TextMeshProUGUI descriptionText;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {   
        titleText = transform.Find("Title").GetComponent<TextMeshProUGUI>();
        descriptionText = transform.Find("Description").GetComponent<TextMeshProUGUI>();
    }
    public void SetTitle(string title)
    {
        this.title = title;
    }

    public void SetIllustration(Sprite illustration)
    {
        this.illustration = illustration;
    }

    public void SetDescription(string description)
    {
        this.description = description;
    }

    public void Show(bool value)
    {
        this.gameObject.SetActive(value);
    }

    public void Reset()
    {
        Show(false);
        title = "";
        description = "";
        illustration = null;
    }

    private void UpdateUI()
    {
        titleText.text = title;
        descriptionText.text = description;
    }

    public void Update()
    {
        UpdateUI();
    }

}
