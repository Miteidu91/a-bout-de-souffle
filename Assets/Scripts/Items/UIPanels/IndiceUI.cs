using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IndiceUI : MonoBehaviour
{
    public static IndiceUI Instance;
    public string title = "";
    public string description = "";

    private TextMeshProUGUI titleText;
    private TextMeshProUGUI descriptionText;

    void Start()
    {
        titleText = transform.Find("Title").GetComponent<TextMeshProUGUI>();
        descriptionText = transform.Find("Description").GetComponent<TextMeshProUGUI>();
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void SetTitle(string title)
    {
        this.title = title;
    }

    public void SetDescription(string description)
    {
        this.description = description;
    }

    public void Show(bool value)
    {
        this.gameObject.SetActive(value);
    }

    public void Reset()
    {
        Show(false);
        title = "";
        description = "";
    }

    private void UpdateUI()
    {
        titleText.text = title;
        descriptionText.text = description;
    }

    public void Update()
    {
        UpdateUI();
    }
}
