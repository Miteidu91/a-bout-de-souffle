using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ObjectUI : MonoBehaviour
{
    public static ObjectUI Instance;
    public string title = "";
    public string description = "";

    private TextMeshProUGUI titleText;
    //TODO: rajouter illustration
    private TextMeshProUGUI descriptionText;

    void Start()
    {
        titleText = transform.Find("Title").GetComponent<TextMeshProUGUI>();
        descriptionText = transform.Find("Description").GetComponent<TextMeshProUGUI>();
    }

    public Sprite illustration;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void SetTitle(string title)
    {
        this.title = title;
    }

    public void SetIllustration(Sprite illustration)
    {
        this.illustration = illustration;
    }

    public void SetDescription(string description)
    {
        this.description = description;
    }

    public void Show(bool value)
    {
        this.gameObject.SetActive(value);
    }

    public void Reset()
    {
        Show(false);
        title = "";
        description = "";
        illustration = null;
    }

    private void UpdateUI()
    {
        titleText.text = title;
        descriptionText.text = description;
    }

    public void Update()
    {
        UpdateUI();
    }
}
