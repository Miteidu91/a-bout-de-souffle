using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Object : Item
{
    public Sprite illustration;
    private GameObject gobj = null;
    public Object()
    {
        itemType = ItemType.Objet;
    }
    public override IEnumerator Interact()
    {
        PickUp();
        if (gobj != null)
        {
            gobj.SetActive(false);
        }
        Debug.Log("object collected " + this.itemName);
        //GameManager.Instance.ObjectObtained.Add(this.itemName);
        CursorManager.Instance.CanInteract = true;
        yield return null;
    }
    public override void ShowDetails()
    {
        ObjectUI.Instance.SetTitle(this.itemName);
        ObjectUI.Instance.SetDescription(this.itemDescription);
        ObjectUI.Instance.SetIllustration(this.illustration);
        ObjectUI.Instance.Show(true);
    }

    
    public void SetGO(GameObject GO)
    {
        gobj = GO;
    }
}
public class ObjectGO : MonoBehaviour
{
    // Start is called before the first frame update
    public Object obj;
    public void Start()
    {
        Initialize();
    }
    public void Initialize()
    {
        obj.SetGO(this.gameObject);
        if (obj.illustration == null)
        {
            IconGenerator ic = IconGenerator.MakeIconGenerator(5);
            Texture2D ns = ic.TakeObjectSnapshot(gameObject);
            Sprite s = Sprite.Create(ns, new Rect(0.0f, 0.0f, ns.width, ns.height), new Vector2(0.5f, 0.5f), 100.0f);
            obj.illustration = s;
        }
    }
}
