using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventaireUIManager : MonoBehaviour
{
    public static InventaireUIManager Instance;
   // public GameObject inventaire;
    public GameObject templateItems;
    public GameObject inventaireObjet;
    public GameObject inventaireIndices;
    public GameObject inventaireFP;
    public Image itemInteract;
    private Transform objetSlotContainer;
    private Transform indicesSlotContainer;
    private Transform fpSlotContainer;
    private Transform itemSlotTemplate;
    //private int nbObjets = 0;
    private bool objetIsOpen=false;
    private bool indiceIsOpen = false;
    private bool fpIsOpen = false;
    //private TextMeshProUGUI titleText;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        OpenInventory();
        templateItems.SetActive(true);
        objetSlotContainer = inventaireObjet.transform.Find("ObjetsContainer");
        indicesSlotContainer = inventaireIndices.transform.Find("IndicesContainer");
        fpSlotContainer = inventaireFP.transform.Find("FPContainer");
        itemSlotTemplate = templateItems.transform;
        //Inventory.Instance.OnItemListChanged += Inventory_OnItemListChanged;
        //RefreshInventoryItems(Inventory.Instance.GetItemList());
        CloseInventory();
        templateItems.SetActive(false);
    }


    


    public void RefreshInventoryObjet()
    {
        int nbObjets = 0;
        foreach (Transform child in objetSlotContainer)
        {
            Destroy(child.gameObject);
        }

        templateItems.SetActive(true);
        foreach (Item item in Inventory.Instance.GetItemList(ItemType.Objet))
        {
            ShowItem(nbObjets,item, objetSlotContainer);
            nbObjets++;
        }
        templateItems.SetActive(false);
    }

    public void RefreshInventoryIndices()
    {
        int nbObjets = 0;
        foreach (Transform child in indicesSlotContainer)
        {
            Destroy(child.gameObject);
        }

        templateItems.SetActive(true);
        foreach (Item item in Inventory.Instance.GetItemList(ItemType.Indice))
        {
            ShowItem(nbObjets, item, indicesSlotContainer);
            nbObjets++;
        }
        templateItems.SetActive(false);
    }

    private void ShowItem(int nbObjets,Item item, Transform container )
    {
        //offsetX = GetOffsetX(nbObjets);
        int offsetX = 0;
        if (nbObjets >= 7) offsetX = 900;
        Transform itemSlot = Instantiate(itemSlotTemplate, itemSlotTemplate.position + new Vector3(offsetX, -115 * (nbObjets % 7), 0), itemSlotTemplate.rotation, container);
        TextMeshProUGUI titleText = itemSlot.Find("Title").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI descriptionText = itemSlot.Find("Description").GetComponent<TextMeshProUGUI>();
        titleText.text = item.getItemName();
        descriptionText.text = item.getItemDescription();
        //itemSlot.GetComponent<Image>().sprite = (item.GetSprite());
        if (item.GetSprite() != null)
        {
            itemSlot.Find("ButtonItem").GetComponentInChildren<Image>().sprite = item.GetSprite();
        }
        itemSlot.Find("ButtonItem").GetComponent<Button>().onClick.AddListener(delegate { GameManager.Instance.Combine(item); });

    }
    private void OpenInventory()
    {
        ShowInventory(inventaireObjet, true);
        ShowInventory(inventaireIndices, true);
        ShowInventory(inventaireFP, true);
        objetIsOpen = true;
        indiceIsOpen = true;
        fpIsOpen = true;
    }

    private void CloseInventory()
    {
        ShowInventory(inventaireObjet, false);
        ShowInventory(inventaireIndices, false);
        ShowInventory(inventaireFP, false);
        objetIsOpen = false;
        indiceIsOpen = false;
        fpIsOpen = false;
        CursorManager.Instance.CanInteract = true;
    }

    private void ShowInventory(GameObject objet, bool actif)
    {
        objet.SetActive(actif);
    }

    public void ButtonObjet()
    {
        if (!objetIsOpen)
        {
            CursorManager.Instance.CanInteract = false;
            objetIsOpen = true;
            indiceIsOpen = false;
            fpIsOpen = false;
            ShowInventory(inventaireObjet, true);
            ShowInventory(inventaireIndices, false);
            ShowInventory(inventaireFP, false);
        }
        else
        {
            CloseInventory();
        }
    }
    public void ButtonIndices()
    {
        if (!indiceIsOpen)
        {
            CursorManager.Instance.CanInteract = false;
            objetIsOpen = false;
            indiceIsOpen = true;
            fpIsOpen = false;
            ShowInventory(inventaireObjet, false);
            ShowInventory(inventaireIndices, true);
            ShowInventory(inventaireFP, false);
        }
        else
        {
            CloseInventory();
        }
    }

    public void ButtonFP()
    {
        if (!fpIsOpen)
        {
            CursorManager.Instance.CanInteract = false;
            objetIsOpen = false;
            indiceIsOpen = false;
            fpIsOpen = true;
            ShowInventory(inventaireObjet, false);
            ShowInventory(inventaireIndices, false);
            ShowInventory(inventaireFP, true);
        }
        else
        {
            CloseInventory();
        }
    }
    /*private int GetOffsetX(int nbObjets)
    {
        int offsetX = 0;
        if (nbObjets >= 7)
        {
            offsetX = 900;
        }
        if (nbObjets >= 14)
        {
            offsetX = 1850;
        }
        if (nbObjets >= 21)
        {
            offsetX = 2750;
        }
        return offsetX;
    }*/


    /*public void ChangePage(bool var)
    {
        if (var)
        {
            inventaireGO.transform.position = new Vector3(inventaireGO.transform.position.x + 1850, inventaireGO.transform.position.y, inventaireGO.transform.position.z);
        }
        else
        {
            inventaireGO.transform.position = new Vector3(inventaireGO.transform.position.x - 1850, inventaireGO.transform.position.y, inventaireGO.transform.position.z);
        }
    }*/

    /////////////////////////////////////////////////////////
    // inspiré de CodeMonkey
    /* public void RefreshInventoryItems()
    {
        foreach (Transform child in itemSlotContainer)
        {
            if (child == itemSlotTemplate) continue;
            Destroy(child.gameObject);
        }

    int x = 0;
    int y = 0;
    Debug.Log("Tets test");
        /*float itemSlotCellSize = 75f;
        foreach (Item item in Inventory.Instance.GetItemList())
        {
            RectTransform itemSlotRectTransform = Instantiate(itemSlotTemplate, itemSlotContainer).GetComponent<RectTransform>();
            itemSlotRectTransform.gameObject.SetActive(true);

            itemSlotRectTransform.GetComponent<Button>().onClick.AddListener(() =>
            {
                item.ShowDetails();
            });

            itemSlotRectTransform.anchoredPosition = new Vector2(x * itemSlotCellSize, -y * itemSlotCellSize);
            Image image = itemSlotRectTransform.Find("image").GetComponent<Image>();
            image.sprite = item.GetSprite();
            x++;
            if (x >= 4)
            {
                x = 0;
                y++;
            }
        }
    }*/

    /*private void Inventory_OnItemListChanged(object sender, System.EventArgs e)
{
    RefreshInventoryItems();
}*/

    /*public void AddItem(Item item)
    {
        int offsetX=0;
        OpenInventory();
        if (nbObjets >= 7)
        {
            offsetX = 900;
        }
        if (nbObjets >= 14)
        {
            offsetX = 1850;
        }
        if(nbObjets >= 21)
        {
            offsetX = 2750;
        }
        templateItems.SetActive(true);
        //itemSlotTemplate = inventaireT.Find("itemSlotTemplate");
        Transform itemSlot = Instantiate(itemSlotTemplate, itemSlotTemplate.position + new Vector3(offsetX, - 115 * (nbObjets%7), 0), itemSlotTemplate.rotation, itemSlotContainer);
        TextMeshProUGUI titleText = itemSlot.Find("Title").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI descriptionText = itemSlot.Find("Description").GetComponent<TextMeshProUGUI>();
        titleText.text = item.getItemName();
        descriptionText.text = item.getItemDescription();
        //itemSlot.GetComponent<Image>().sprite = (item.GetSprite());
        nbObjets++;
        templateItems.SetActive(false);
        CloseInventory();
    }*/

}
