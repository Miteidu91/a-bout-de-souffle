using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

[System.Serializable]
public class Door : Item
{
    public List<GameObject> externalWalls;
    public int destination = 0;
    private bool inside = false;
    public NavMeshLink nml;
    public NavMeshAgent agent;
    public Door()
    {
        itemType = ItemType.Door;
    }
    public override IEnumerator Interact()
    {
        //SceneManager.LoadScene(destination);
        nml.gameObject.SetActive(true);
        inside = !inside;
        if (inside)
        {
            agent.SetDestination(nml.gameObject.transform.position + nml.endPoint);
        }
        else
        {
            agent.SetDestination(nml.gameObject.transform.position + nml.startPoint);
        }


        if (inside)
        {
            CameraController2.Instance.Fixe(externalWalls);
        }
        else
        {
            CameraController2.Instance.FollowPlayer();
        }

        foreach (GameObject g in externalWalls)
        {
            g.GetComponent<Renderer>().enabled = true;
            foreach(Material m in g.GetComponent<Renderer>().materials)
            {
                ToFadeMode(m);
            }
            //ToFadeMode(g.GetComponent<Renderer>().material);
        }
        for (float f = 1f; f > 0; f -= Time.deltaTime)
        {
            foreach (GameObject g in externalWalls)
            {
                fade(f, g, inside);
            }
            yield return null;
        }
        foreach (GameObject g in externalWalls)
        {
            if (inside)
            {
                g.GetComponent<Renderer>().enabled = false;
            }
            else
            {
                foreach (Material m in g.GetComponent<Renderer>().materials)
                {
                    ToOpaqueMode(m);
                }
                //ToOpaqueMode(g.GetComponent<Renderer>().material);
                Color c = g.GetComponent<Renderer>().material.color;
                c = new Color(c.r, c.g, c.b, 1);
                g.GetComponent<Renderer>().material.color = c;
            }
        }


        nml.gameObject.SetActive(false);
        CursorManager.Instance.CanInteract = true;
        yield return null;
    }
    public override void ShowDetails()
    {
        ObjectUI.Instance.SetTitle(this.itemName);
        ObjectUI.Instance.SetDescription(this.itemDescription);
        ObjectUI.Instance.Show(true);
    }

    private void fade(float f, GameObject g, bool toTransparent)
    {
        float newAlpha;
        if (toTransparent)
        {
            newAlpha = f;
        }
        else
        {
            newAlpha = 1-f;
        }
        //Color c = g.GetComponent<Renderer>().material.color;
        Material[] m = g.GetComponent<Renderer>().materials;
        for(int i = 0; i < m.Length; i++)
        {
            Color c = m[i].color;
            c = new Color(c.r, c.g, c.b, newAlpha);
            m[i].color = c;
            //g.GetComponent<Renderer>().material.color = c;
        }
        //g.GetComponent<Renderer>().materials = m;
    }
    private void ToOpaqueMode(Material material)
    {
        material.SetOverrideTag("RenderType", "");
        material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
        material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
        material.SetInt("_ZWrite", 1);
        material.DisableKeyword("_ALPHATEST_ON");
        material.DisableKeyword("_ALPHABLEND_ON");
        material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        material.renderQueue = -1;
    }

    private void ToFadeMode(Material material)
    {
        material.SetOverrideTag("RenderType", "Transparent");
        material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        material.SetInt("_ZWrite", 0);
        material.DisableKeyword("_ALPHATEST_ON");
        material.EnableKeyword("_ALPHABLEND_ON");
        material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;
    }
}
public class DoorGO : MonoBehaviour
{
    // Start is called before the first frame update
    public Door door;
    void Start()
    {

    }
}
