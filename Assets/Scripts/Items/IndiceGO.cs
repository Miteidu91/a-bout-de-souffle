using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Indice : Item
{
    public Sprite illustration;
    public Indice()
    {
        itemType = ItemType.Indice;
    }
    public override IEnumerator Interact()
    {
        PickUp();
        Debug.Log("clue collected " + this.itemName);
        //GameManager.Instance.ClueObtained.Add(this.itemName);
        CursorManager.Instance.CanInteract = true;
        yield return null;
    }
    public override void ShowDetails()
    {
        IndiceUI.Instance.SetTitle(this.itemName);
        IndiceUI.Instance.SetDescription(this.itemDescription);
        IndiceUI.Instance.Show(true);
    }
}
public class IndiceGO : MonoBehaviour
{
    // Start is called before the first frame update
    public Indice indice;
    public void Start()
    {
        Initialize();
    }
    public void Initialize()
    {

        if (indice.illustration == null)
        {
            IconGenerator ic = IconGenerator.MakeIconGenerator(5);
            Texture2D ns = ic.TakeObjectSnapshot(gameObject);
            Sprite s = Sprite.Create(ns, new Rect(0.0f, 0.0f, ns.width, ns.height), new Vector2(0.5f, 0.5f), 100.0f);
            indice.illustration = s;
        }
    }
}
