using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

[System.Serializable]
public class FichePersonnage : Item
{
    public Sprite charsprite;

    //public DialogueUI showD;
    public Dialogue dialogue { get; set; }
    public FichePersonnage()
    {
        itemType = ItemType.FichePersonnage;
    }
    public override IEnumerator Interact()
    {
        Debug.Log("Starting Dialogue with " + itemName);
        yield return DialogueManager.Instance.MakeDialogue(dialogue);
    }
    public override void ShowDetails()
    {
        FPUI.Instance.SetTitle(this.itemName);
        FPUI.Instance.SetDescription(this.itemDescription);
        FPUI.Instance.SetIllustration(this.charsprite);
        FPUI.Instance.Show(true);
    }

}
public class FichePersonnageGO : MonoBehaviour
{
    // Start is called before the first frame update
    public FichePersonnage FP = new FichePersonnage();
    public string DialogueName;
    public List<IndiceGO> indiceToRecieve = new List<IndiceGO>();
    public List<ObjectGO> objectToRecieve = new List<ObjectGO>();
    public string idLineGiveItem;
    public List<Action> actionToRecieve = new List<Action>();
    public string idLineGiveAction;
    public void Start()
    {
        FP.dialogue = DialogueManager.Instance.ParseFileCSV(DialogueName);
        FP.dialogue.IdLineGiveItem = idLineGiveItem;
        FP.dialogue.ItemGiven = new List<Item>();
        foreach (ObjectGO objGO in objectToRecieve)
        {
            objGO.Initialize();
            FP.dialogue.ItemGiven.Add(objGO.obj);
        }
        foreach (IndiceGO indiceGO in indiceToRecieve)
        {
            indiceGO.Initialize();
            FP.dialogue.ItemGiven.Add(indiceGO.indice);
        }
        FP.dialogue.IdLineGiveAction = idLineGiveAction;
        FP.dialogue.ActionGiven = actionToRecieve;
    }

}