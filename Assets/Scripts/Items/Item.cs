using UnityEngine;
using System.Collections;

public enum ItemType
{
    Indice,
    Objet,
    FichePersonnage,
    Door
}

[System.Serializable]
public abstract class Item
{
    public int itemID;
    protected ItemType itemType;
    public string itemName;
    public string itemDescription;

    public void PickUp()
    {
        Inventory.Instance.AddItem(this);
    }

    public Sprite GetSprite()
    {
        switch (itemType)
        {
            case ItemType.Indice:
                return ((Indice)this).illustration;
            case ItemType.Objet:
                return ((Object)this).illustration;
            case ItemType.FichePersonnage:
                return ((FichePersonnage)this).charsprite;
            default:
                return ItemAssets.Instance.Unknown;
        }
    }

    public string getItemName()
    {
        return this.itemName;
    }
    public string getItemDescription()
    {
        return this.itemDescription;
    }


    public abstract IEnumerator Interact();

    public abstract void ShowDetails();

    public ItemType GetItemType()
    {
        return itemType;
    }
}
