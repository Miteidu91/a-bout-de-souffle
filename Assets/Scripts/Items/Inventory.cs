using UnityEngine;
using System;
using System.Collections.Generic;
class Inventory : MonoBehaviour
{
    public static Inventory Instance;
    public event EventHandler OnItemListChanged;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    //public Dictionary<int, Item> items = new Dictionary<int, Item>();

    public Dictionary<int, Item> objets = new Dictionary<int, Item>();
    public Dictionary<int, Item> indices = new Dictionary<int, Item>();
    public Dictionary<int, Item> fichePersos = new Dictionary<int, Item>();

    /*public Item GetItem(int itemID)
    {
        return items[itemID];
    }*/
    public void AddItem(Item item)
    {
        Debug.Log("Adding " + item.itemID + " in inventory");
        //InventaireUIManager.Instance.
        if (!objets.ContainsKey(item.itemID) && !indices.ContainsKey(item.itemID) && !fichePersos.ContainsKey(item.itemID))
        {
            switch (item.GetItemType())
            {
                case ItemType.Objet:
                    objets.Add(item.itemID, item);
                    InventaireUIManager.Instance.RefreshInventoryObjet();
                    GameManager.Instance.ObjectObtained.Add(item.itemName);
                    break;
                case ItemType.Indice:
                    indices.Add(item.itemID, item);
                    InventaireUIManager.Instance.RefreshInventoryIndices();
                    GameManager.Instance.ClueObtained.Add(item.itemName);
                    break;
                case ItemType.FichePersonnage:
                    fichePersos.Add(item.itemID, item);
                    break;
            }
        }
        //items.Add(item.itemID, item);
        //InventaireUIManager.Instance.RefreshInventoryItems(GetItemList());
        //InventaireUIManager.Instance.AddItem(item);
    }
    public void RemoveItem(Item item)
    {
        Debug.Log("Removing " + item.itemID + " in inventory");
        //items.Remove(item.itemID);
        switch (item.GetItemType())
        {
            case ItemType.Objet:
                objets.Remove(item.itemID);
                InventaireUIManager.Instance.RefreshInventoryObjet();
                GameManager.Instance.ObjectObtained.Remove(item.itemName);
                break;
            case ItemType.Indice:
                indices.Remove(item.itemID);
                InventaireUIManager.Instance.RefreshInventoryIndices();
                GameManager.Instance.ClueObtained.Remove(item.itemName);
                break;
            case ItemType.FichePersonnage:
                fichePersos.Remove(item.itemID);
                break;
        }
    }


    public Item GetItem(int id)
    {
        if (objets.ContainsKey(id))
        {
            return objets[id];
        }
        else if (indices.ContainsKey(id))
        {
            return indices[id];
        }
        else if (fichePersos.ContainsKey(id))
        {
            return fichePersos[id];
        }
        else
        {
            Debug.Log(objets.Keys);
            Debug.Log(id);
            Debug.LogWarning("no item find in inventory");
            return null;
        }
    }
    /*public void RemoveItem(int itemID)
    {
        items.Remove(itemID);
    }*/

    public void RemoveAllItems()
    {
        //items.Clear();
        objets.Clear();
        indices.Clear();
        fichePersos.Clear();
    }

    public List<Item> GetItemList(ItemType type)
    {
        //return new List<Item>(items.Values);
        switch (type)
        {
            case ItemType.Objet:
                return new List<Item>(objets.Values);
                break;
            case ItemType.Indice:
                return new List<Item>(indices.Values);
                break;
            case ItemType.FichePersonnage:
                return new List<Item>(fichePersos.Values);
                break;
        }
        return null;
    }
}
