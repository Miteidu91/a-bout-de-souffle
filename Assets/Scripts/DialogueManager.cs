using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;

public abstract class DialogueCondition
{
    public abstract bool IsRespected();
    public abstract string GetInfos();
}
public class LineCondition : DialogueCondition
{
    public string Line { get; set; }
    public bool ToSpeak { get; set; }
    public LineCondition(string lid, bool toSpeak)
    {
        Line = lid;
        ToSpeak = toSpeak;
    }
    public override bool IsRespected()
    {
        return (GameManager.Instance.LineObtained.Contains(Line) == ToSpeak);
    }
    public override string GetInfos()
    {
        return "LC-" + Line + "-" + ToSpeak.ToString();
    }
}
public class ObjectCondition : DialogueCondition
{
    public string Object { get; set; }
    public bool ToHave { get; set; }
    public ObjectCondition(string oid, bool toHave)
    {
        Object = oid;
        ToHave = toHave;
    }
    public override bool IsRespected()
    {
        return (GameManager.Instance.ObjectObtained.Contains(Object) == ToHave);
    }
    public override string GetInfos()
    {
        return "OC-" + Object + "-" + ToHave.ToString();
    }
}
public class ClueCondition : DialogueCondition
{
    public string Clue { get; set; }
    public bool ToHave { get; set; }
    public ClueCondition(string cid, bool toHave)
    {
        Clue = cid;
        ToHave = toHave;
    }
    public override bool IsRespected()
    {
        return (GameManager.Instance.ClueObtained.Contains(Clue) == ToHave);
    }
    public override string GetInfos()
    {
        return "CC-" + Clue + "-" + ToHave.ToString();
    }
}
public class Line
{
    private Regex LinesId = new Regex("([^\n]+)");
    private Regex LinesChId = new Regex("\tCharId:([^\n]+)");
    private Regex LinesSp = new Regex("\tSpoken:([^\n]+)");
    private Regex LinesSl = new Regex("\tSilent:([^\n]+)");
    private Regex LinesEnd = new Regex("\tEnd:([^\n]+)");
    private Regex LinesTxt = new Regex("\tText:([^\n]+)");
    private string Silence = "...";
    public bool Spoken { get; set; }
    public bool End { get; set; }
    public bool Silent { get; set; }
    public string CharacterId { get; set; }
    public string LineId { get; set; }
    public string Text { get; set; }
    public List<Line> NextLines { get; set; }
    public List<DialogueCondition> Conditions { get; set; }
    public Line(string textFromFile)
    {
        Match mId = LinesId.Match(textFromFile);
        Match mChId = LinesChId.Match(textFromFile);
        Match mSp = LinesSp.Match(textFromFile);
        Match mSl = LinesSl.Match(textFromFile);
        Match mEnd = LinesEnd.Match(textFromFile);
        Match mTxt = LinesTxt.Match(textFromFile);
        LineId = mId.Value.Replace("\r", "");
        CharacterId = mChId.Value.Replace("\tCharId:", "").Replace("\r", "");
        Spoken = bool.Parse(mSp.Value.Replace("\tSpoken:", ""));
        Silent = bool.Parse(mSl.Value.Replace("\tSilent:", ""));
        End = bool.Parse(mEnd.Value.Replace("\tEnd:", ""));
        NextLines = new List<Line>();
        Conditions = new List<DialogueCondition>();
        if (Silent)
        {
            Text = Silence;
        }
        else
        {
            Text = mTxt.Value.Replace("\tText:", "").Replace("\r", "");
        }
    }
    public Line(string textFromFile, bool isCSV)
    {
        string[] informations = textFromFile.Split(',');
        if (informations[2] != "")
        {
            LineId = informations[2].Replace("\r", "");
            CharacterId = informations[0].Replace("\r", "");
            Spoken = false;
            Silent = bool.Parse(informations[4]);
            End = bool.Parse(informations[5]);

            NextLines = new List<Line>();
            Conditions = new List<DialogueCondition>();
            if (Silent)
            {
                Text = Silence;
            }
            else
            {
                Text = informations[7].Replace("\r", "").Replace(";", ",");
            }
            //Debug.Log(GetInfos());
        }
        else
        {
            LineId = null;
        }
    }
    public Line(string charId, string lineId, string text, List<DialogueCondition> cond)
    {
        Spoken = false;
        Silent = false;
        End = false;
        CharacterId = charId;
        LineId = lineId;
        Text = text;
        NextLines = new List<Line>();
        Conditions = cond;
    }
    public bool IsActivable()
    {
        bool conditionVerified = true;
        foreach (DialogueCondition dc in Conditions)
        {
            conditionVerified = conditionVerified && dc.IsRespected();
        }
        return conditionVerified;
    }
    public bool hasMultiplePath()
    {
        return NextLines.Count > 1;
    }
    /*
    public void GetClueFromLine()
    {
        if (isClue)
        {
            Indice i = new Indice();
            i.itemID = 1000 + ClueId;
            i.itemName = CharacterId;
            i.itemDescription = Text;

            string directoryPath = "Assets\\Sprites\\Personnage";

            string path = Path.Combine(directoryPath, CharacterId);
            path = path + ".png";
            i.illustration = (Sprite)AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D));

            Inventory.Instance.AddItem(i);
        }
    }
    */
    public string GetInfos()
    {
        StringBuilder sb = new StringBuilder("", 100);
        sb.Append(LineId);
        sb.Append("\n\tCharId:" + CharacterId);
        sb.Append("\n\tSpoken:" + Spoken.ToString());
        sb.Append("\n\tSilent:" + Silent.ToString());
        sb.Append("\n\tEnd:" + End.ToString());
        sb.Append("\n\tNextLines:");
        foreach (Line l in NextLines)
        {
            sb.Append(l.LineId + ",");
        }
        sb.Append("\n\tConditions:");
        foreach (DialogueCondition cond in Conditions)
        {
            sb.Append(cond.GetInfos() + ",");
        }
        sb.Append("\n\tText:" + Text);
        return sb.ToString();
    }
    public bool Equal(Line l)
    {
        if (l == null)
        {
            return false;
        }
        else
        {
            return l.LineId == LineId;
        }
    }
}
public class Dialogue
{
    private List<Line> SaidLines { get; set; }
    public List<Line> CurrentLines { get; set; }
    public bool Pause { get; set; }
    public List<Item> ItemGiven { get; set; }
    public List<Action> ActionGiven { get; set; }
    public string IdLineGiveItem { get; set; }
    public string IdLineGiveAction { get; set; }
    private Regex LinesParts = new Regex("([^\n]+)(([\n][\t]([^\n]+))+)");
    private Regex LinesNL = new Regex("\tNextLines:([^\n]*)");
    private Regex LinesCnd = new Regex("\tConditions:([^\n]*)");
    private Regex LinesPartsCSV = new Regex("([^\n]+)");


    public Dialogue(string textFromFile)
    {
        List<string> l = new List<string>();
        List<Line> line = new List<Line>();
        MatchCollection m = LinesParts.Matches(textFromFile);
        MatchCollection mNL = LinesNL.Matches(textFromFile);
        MatchCollection mCnd = LinesCnd.Matches(textFromFile);
        foreach (Match match in m)
        {
            l.Add(match.Value);
        }
        foreach (string ltext in l)
        {
            Line L = new Line(ltext);
            line.Add(L);
        }
        for (int i = 0; i < m.Count; i++)
        {
            string[] lids = mNL[i].Value.Replace("\tNextLines:", "").Replace("\r", "").Split(',');
            string[] conds = mCnd[i].Value.Replace("\tConditions:", "").Replace("\r", "").Split(',');
            foreach (string condText in conds)
            {
                if (condText.StartsWith("LC-"))
                {
                    string c = condText.Replace("LC-", "");
                    string[] condlparts = c.Split('-');
                    //Line LC = line.Find(l => l.LineId == condlparts[0]);
                    bool ToSpeak = bool.Parse(condlparts[1]);
                    line[i].Conditions.Add(new LineCondition(condlparts[0], ToSpeak));
                }
                else if (condText.StartsWith("OC-"))
                {
                    string c = condText.Replace("OC-", "");
                    string[] condlparts = c.Split('-');
                    bool ToHave = bool.Parse(condlparts[1]);
                    line[i].Conditions.Add(new ObjectCondition(condlparts[0], ToHave));
                }
                else if (condText.StartsWith("CC-"))
                {
                    string c = condText.Replace("CC-", "");
                    string[] condlparts = c.Split('-');
                    bool ToHave = bool.Parse(condlparts[1]);
                    line[i].Conditions.Add(new ClueCondition(condlparts[0], ToHave));
                }
            }
            foreach (string lid in lids)
            {
                Line NL = line.Find(l => l.LineId == lid);
                if (NL != null)
                {
                    line[i].NextLines.Add(NL);
                }
            }
        }
        SaidLines = new List<Line>();
        CurrentLines = new List<Line> { line[0] };
        Pause = false;
    }
    public Dialogue(string textFromFile, bool isCSV)
    {
        List<string> l = new List<string>();
        List<Line> line = new List<Line>();
        MatchCollection m = LinesPartsCSV.Matches(textFromFile);
        
        for(int i = 1; i < m.Count; i++)
        {
            if (m[i].Value.Split(',')[2] != "")
            {
                l.Add(m[i].Value);
            }
        }

        foreach (string ltext in l)
        {
            Line L = new Line(ltext,true);
            if (L.LineId != null)
            {
                line.Add(L);
            }
        }
        for (int i = 0; i < l.Count; i++)
        {
            string[] lids = l[i].Split(',')[6].Replace("\r", "").Split(';');
            string[] conds = l[i].Split(',')[3].Replace("\r", "").Split(';');
            foreach (string condText in conds)
            {
                if (condText.StartsWith("LC-"))
                {
                    //string c = condText.Replace("LC-", "");
                    //c = condText.Replace(" ", "");
                    string[] condlparts = condText.Split('-');
                    //Line LC = line.Find(l => l.LineId == condlparts[0]);
                    bool ToSpeak = bool.Parse(condlparts[2]);
                    line[i].Conditions.Add(new LineCondition(condlparts[1], ToSpeak));
                }
                else if (condText.StartsWith("OC-"))
                {
                    //string c = condText.Replace("OC-", "");
                    //c = condText.Replace(" ", "");
                    string[] condlparts = condText.Split('-');
                    bool ToHave = bool.Parse(condlparts[2]);
                    line[i].Conditions.Add(new ObjectCondition(condlparts[1], ToHave));
                }
                else if (condText.StartsWith("CC-"))
                {
                    //string c = condText.Replace("CC-", "");
                    //c = condText.Replace(" ", "");
                    string[] condlparts = condText.Split('-');
                    bool ToHave = bool.Parse(condlparts[2]);
                    line[i].Conditions.Add(new ClueCondition(condlparts[1], ToHave));
                }
            }
            foreach (string lid in lids)
            {
                string lid2 = lid.Replace(" ", "");
                Line NL = line.Find(l => l.LineId == lid2);
                if (NL != null)
                {
                    line[i].NextLines.Add(NL);
                }
            }
        }
        SaidLines = new List<Line>();
        CurrentLines = new List<Line> { line[0] };
        Pause = false;
    }
    public Dialogue(Line l)
    {
        SaidLines = new List<Line> { };
        CurrentLines = new List<Line> { l };
    }
    public Line Choose(int l)
    {
        if (l >= 0 && l < CurrentLines.Count && CurrentLines[l].IsActivable())
        {
            Line line = CurrentLines[l];
            CurrentLines = line.NextLines;
            SaidLines.Add(line);
            //line.GetClueFromLine();
            GameManager.Instance.LineObtained.Add(line.LineId);
            Debug.Log(line.CharacterId + " : " + line.Text);
            line.Spoken = true;
            Pause = line.End;
            GetItem(line);
            //GetAction(line);
            return line;
        }
        else
        {
            return null;
        }
    }
    public Line Choose()
    {
        Line line = CurrentLines.Find(l => l.IsActivable());
        CurrentLines = line.NextLines;
        SaidLines.Add(line);
        //line.GetClueFromLine();
        GameManager.Instance.LineObtained.Add(line.LineId);
        Debug.Log(line.CharacterId + " : " + line.Text);
        line.Spoken = true;
        Pause = line.End;
        GetItem(line);
        //GetAction(line);
        return line;
    }
    public Line ChooseSilence()
    {
        Line line = CurrentLines.Find(l => l.IsActivable() && l.Silent);
        if (line == null)
        {
            Debug.Log("error, pas de r�plique silence accessible");
        }
        else
        {
            CurrentLines = line.NextLines;
            SaidLines.Add(line);
            //line.GetClueFromLine();
            GameManager.Instance.LineObtained.Add(line.LineId);
            Debug.Log(line.CharacterId + " : " + line.Text);
            line.Spoken = true;
            Pause = line.End;
        }
        GetItem(line);
        //GetAction(line);
        return line;
    }
    public void GetItem(Line l)
    {
        if (IdLineGiveItem != null && l!=null && l.LineId == IdLineGiveItem)
        {
            foreach(Item it in ItemGiven)
            {
                it.PickUp();
            }
        }
    }

    public bool HasSilence()
    {
        return (CurrentLines.Find(l => l.IsActivable() && l.Silent) != null);
    }
    public bool HasMultiplePath()
    {
        return CurrentLines.Count > 1;
    }
    public bool HasMultiplePathAvailable()
    {
        List<Line> Possibilities = CurrentLines.FindAll(l => l.IsActivable());
        return Possibilities.Count > 1;
    }
    public bool IsFinished()
    {
        return CurrentLines.Count == 0;
    }
    public bool IsFinishedWithAvailables()
    {
        List<Line> Possibilities = CurrentLines.FindAll(l => l.IsActivable());
        return Possibilities.Count == 0;
    }
    public List<Line> LineAvailable()
    {
        List<Line> alines = new List<Line>();
        foreach(Line l in CurrentLines)
        {
            if (l.IsActivable())
            {
                alines.Add(l);
            }
        }
        return alines;
    }
    public void PrintInfos()
    {
        string s = "";
        for (int i = 0; i < CurrentLines.Count; i++)
        {
            s += i.ToString() + " : " + CurrentLines[i].Text + " | ";
        }
        Debug.Log("Choose : " + s);
    }
}

public class DialogueManager : MonoBehaviour
{
    // Start is called before the first frame update
    //private string directoryPath = "Assets\\StreamingAssets\\Dialogues";
    //private Dialogue test;
    public static DialogueManager Instance;

    public DialogueUI dialogueUI;

    private bool waitPlayer;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    /*
    void Start()
    {
        Line L1 = new Line("a", "L1", "Salut Bertrand!", new List<DialogueCondition>());
        Line L2 = new Line("b", "L2", "Salut Albert!", new List<DialogueCondition>());
        Line L3a = new Line("a", "L3a", "Comment a va ?", new List<DialogueCondition>());
        Line L3b = new Line("a", "L3b", "O est le pain ?", new List<DialogueCondition>());
        Line L4a = new Line("b", "L4a", "a va, et toi ?", new List<DialogueCondition>());
        Line L4b1 = new Line("b", "L4b1", "Je n'en sais rien, tu pourais me dire comment a va avant !", new List<DialogueCondition> { new LineCondition(L3a, false) });
        Line L4b2 = new Line("b", "L4b2", "Je pense qu'il est dans la cuisine", new List<DialogueCondition> { new LineCondition(L3a, true) });
        Line L5a = new Line("a", "L5a", "a va!", new List<DialogueCondition>());
        Line L6a = new Line("b", "L6", "okay", new List<DialogueCondition>());

        L1.NextLines = new List<Line> { L2 };
        L2.NextLines = new List<Line> { L3a, L3b };
        L3a.NextLines = new List<Line> { L4a };
        L3b.NextLines = new List<Line> { L4b1, L4b2 };
        L4b1.NextLines = new List<Line> { L3a, L3b };
        L4b2.NextLines = new List<Line>();
        L4a.NextLines = new List<Line> { L5a };
        L5a.NextLines = new List<Line> { L6a };
        L6a.NextLines = new List<Line> { L3a, L3b };
        

        //Dialogue D = new Dialogue(L1);
        //string fileName = "Dialogue1.txt";
        //string path = Path.Combine(directoryPath, fileName);
        //CreateDialogueFile(path,D);
        
        string fileNameEx = "Exemple.txt";
        string pathEx = Path.Combine(directoryPath, fileNameEx);
        test = ParseFile(pathEx);
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(MakeDialogue(test));
        }

    }
    */

    //fonctions simulant un dialogue
    private IEnumerator LetPlayerChoose(Dialogue D)
    {
        int choice = -1;
        Line line;
        dialogueUI.visibleBarreCountdown(D.HasSilence());
        float duration = 6f; // 3 seconds you can change this to whatever you want
        float normalizedTime = 0;
        while (choice < 0 && (normalizedTime <= 1f || !D.HasSilence()))
        {
            choice = GetChoice();
            if (D.HasSilence())
            {
                dialogueUI.ImageBarreCountdown.fillAmount = 1f - normalizedTime;
                normalizedTime += Time.deltaTime / duration;
            }
            yield return null;
        }
        if (normalizedTime >= 1f && D.HasSilence()) 
        {
            line = D.ChooseSilence();
            GetAction(line, D);
            Debug.Log(line.Text);
        }
        else
        {
            line = D.Choose(choice);
            GetAction(line, D);
        }     
        dialogueUI.visibleBarreCountdown(false);
        //On ferme les réponses
        dialogueUI.CloseResponses();
        //On affiche la réponse au joueur
        dialogueUI.showDialogue(line);
    }

    /*private int GetChoice()
    {
        for (int i = 0; i < 10; i++)
        {
            if (Input.GetKey((KeyCode)(256 + i)))
            {
                return i;
            }
        }
        return -1;
    }*/
    public void GetAction(Line l, Dialogue D)
    {
        if (D.IdLineGiveAction != null && l != null && l.LineId == D.IdLineGiveAction)
        {
            foreach (Action ac in D.ActionGiven)
            {
                StartCoroutine(ac.Activate());
            }
        }
    }
    private int GetChoice()
    {
        if (Input.GetKey("0"))
        {
            Debug.Log(0);
            return 0;
        }
        else if (Input.GetKey("1"))
        {
            Debug.Log(1);
            return 1;
        }
        else if (Input.GetKey("2"))
        {
            Debug.Log(2);
            return 2;
        }
        else if (Input.GetKey("3"))
        {
            Debug.Log(3);
            return 3;
        }
        else if (Input.GetKey("4"))
        {
            Debug.Log(4);
            return 4;
        }
        else
        {
            return -1;
        }
    }
    public IEnumerator MakeDialogue(Dialogue D)
    {
        if (D != null)
        {
            Debug.Log("starting dialogue " + D.CurrentLines[0].CharacterId);
            dialogueUI.visibileDialogue(true);
            D.Pause = false;
            Line line;
            if (!D.IsFinishedWithAvailables())
            {
                if (!D.HasMultiplePathAvailable())
                {
                    line = D.Choose();
                    GetAction(line, D);
                    dialogueUI.showDialogue(line);
                    waitPlayer = false;
                    yield return WaitNextLine();
                }
                else
                {
                    D.PrintInfos();
                    dialogueUI.showResponses(D.LineAvailable());
                    //dialogueUI.showResponses(D);
                    yield return LetPlayerChoose(D);
                    waitPlayer = false;
                    yield return WaitNextLine();
                }
                if (!D.Pause)
                {
                    //yield return new WaitForSeconds(2);
                    yield return MakeDialogue(D);
                }
            }
            if (GameManager.Instance.LineObtained.Contains(D.IdLineGiveItem))
            {
                foreach(Item it in D.ItemGiven)
                {
                    Inventory.Instance.AddItem(it);
                }
            }
            CursorManager.Instance.CanInteract = true;
            dialogueUI.visibileDialogue(false);
        }
        else
        {
            Debug.LogWarning("Null dialog called");
        }
    }

    public void WaitForPlayer()
    {
        Debug.Log("Button press");
        waitPlayer = true;
    }

    private IEnumerator WaitNextLine()
    {
        while (!waitPlayer)
        {
            yield return null;
        }
    }



    //fonctions gerant les fichiers txt
    private void CreateDialogueFile(string path, Dialogue D)
    {
        if (!File.Exists(path))
        {
            var sr = File.CreateText(path);
            List<Line> Lines = ExploreDialogue(new List<Line>(D.CurrentLines), new List<Line>());
            foreach (Line l in Lines)
            {
                sr.WriteLine(l.GetInfos());
            }
            sr.Close();
        }
    }
    private List<Line> ExploreDialogue(List<Line> currentLines, List<Line> exploration)
    {
        if (currentLines.Count == 0)
        {
            return exploration;
        }
        else
        {
            Line LineExplored = currentLines[0];
            if (!exploration.Contains(LineExplored))
            {
                exploration.Add(LineExplored);
                currentLines.AddRange(LineExplored.NextLines);
            }
            currentLines.RemoveAt(0);
            return ExploreDialogue(currentLines, exploration);
        }
    }
    public Dialogue ParseFile(string path)
    {
        if (File.Exists(path))
        {
            string rawText = File.ReadAllText(path);
            Dialogue D = new Dialogue(rawText);
            return D;
        }
        else
        {
            return null;
        }
    }
    public Dialogue ParseFileCSV(string name)
    {
        string directoryPath = Path.Combine(Application.streamingAssetsPath, "Dialogues");
        //"Assets\\StreamingAssets\\Dialogues";
        string path = Path.Combine(directoryPath, name);


        if (File.Exists(path))
        {
            string rawText = File.ReadAllText(path);
            Dialogue D = new Dialogue(rawText,true);
            return D;
        }
        else
        {
            Debug.LogWarning("no file at path" + path);
            return null;
        }
    }
}
