using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

public class PlayerControllerMesh : MonoBehaviour
{

    public Camera cam;

    public NavMeshAgent agent;

    //public ThirdPersonCharacter character;
    void Start()
    {
        agent.updateRotation = false;
    }
    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetMouseButtonDown(0))
        {
            Debug.Log("Clicked");
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit) && hit.collider.tag == "Ground")
            {
                agent.SetDestination(hit.point);
            }

            //if (agent.remainingDistance > agent.stoppingDistance)
            //{
            //    character.Move(agent.desiredVelocity, false, false);
            //}
            //else
            //{
            //    character.Move(Vector3.zero, false, false);
            //}
        }
        
    }

    public void MoveTo(Vector3 destination)
    {
        agent.SetDestination(destination);
    }
}
