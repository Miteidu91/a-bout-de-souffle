using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public abstract class Action : MonoBehaviour
{
    public List<int> ItemToDiscard;
    public abstract IEnumerator Activate();
    public void Discard()
    {
        foreach(int i in ItemToDiscard)
        {
            Debug.Log("Discarding " + i);
            Item itToDiscard = Inventory.Instance.GetItem(i);
            if (itToDiscard != null)
            {
                Debug.Log(itToDiscard.itemName + " " + itToDiscard.itemID);
                Inventory.Instance.RemoveItem(itToDiscard);
            }
            else
            {
                Debug.LogWarning("try removing null item");
            }
        }
    }
}
[System.Serializable]
public class Interaction
{
    public int item1;
    public int item2;
    public List<Action> actions = new List<Action>();
    public Interaction(int it1,int it2)
    {
        item1 = it1;
        item2 = it2;
        actions = new List<Action>();
    }
}


public class InteractionManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static InteractionManager Instance;
    private Dictionary<(int, int), List<Action>> interaction = new Dictionary<(int, int), List<Action>>();
    public List<Interaction> interactionList = new List<Interaction>();
    public string ErrorDialogName;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        foreach(Interaction it in interactionList)
        {
            interaction.Add((it.item1, it.item2), it.actions);
        }
    }

    public bool Interact(Item it1, Item it2)
    {
        bool DoInteract = interaction.ContainsKey((it1.itemID, it2.itemID)) || interaction.ContainsKey((it2.itemID, it1.itemID));
        Debug.Log("interact" + it1.itemName +"("+it1.itemID+")" + " " + it2.itemName+"(" + it2.itemID + ")"+" " + DoInteract);
        if (interaction.ContainsKey((it1.itemID, it2.itemID))){
            foreach (Action a in interaction[(it1.itemID, it2.itemID)])
            {
                StartCoroutine(a.Activate());
                a.Discard();
            }
        }
        if (interaction.ContainsKey((it2.itemID, it1.itemID)))
        {
            foreach (Action a in interaction[(it2.itemID, it1.itemID)])
            {
                StartCoroutine(a.Activate());
                a.Discard();
            }
        }
        if (!DoInteract)
        {
            Dialogue dialogue = DialogueManager.Instance.ParseFileCSV(ErrorDialogName);
            CursorManager.Instance.CanInteract = false;
            StartCoroutine(DialogueManager.Instance.MakeDialogue(dialogue));
        }
        interaction.Remove((it2.itemID, it1.itemID));
        interaction.Remove((it1.itemID, it2.itemID));
        return DoInteract;
    }

}
