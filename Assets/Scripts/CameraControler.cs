using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour
{
    public GameObject player;
    private Vector3 offsetCamera  =  new Vector3(9.5f, 5, 5);
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //transform.position = player.transform.position + offsetCamera;
        transform .position = new Vector3(0, 0, player.transform.position.z) + offsetCamera;
    }
}
