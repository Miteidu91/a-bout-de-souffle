using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuButtonController : MonoBehaviour
{
    public GameObject pauseMenu;
    public bool isPaused = false;

    void  Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !isPaused)
            pauseGame();
        else if (Input.GetKeyDown(KeyCode.Escape) && isPaused)
            resumeGame();
    }
    public void quitButtonClick()
    {
        Time.timeScale = 1;
        isPaused = false;
        SceneManager.LoadScene(0);
    }

    public void resumeGame()
    {
        Time.timeScale = 1;
        isPaused = false;
        pauseMenu.SetActive(false);
    }

    public void pauseGame()
    {
        Time.timeScale = 0;
        isPaused = true;
        pauseMenu.SetActive(true);
    }
}
