using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;



public class CreateDialogue : MonoBehaviour
{

    private string directoryPath = "Assets\\StreamingAssets\\Dialogues";
    private Dialogue D;
    public Image countdownImage;



    // Start is called before the first frame update
    void Start()
    {
        //boolChrono = false;
        NewDialogue("Exemple.txt");
        //StartCoroutine(Countdown());
        //chrono = 10;
        //Debug.Log("test");
        //string path = Path.Combine(directoryPath, "Exemple.txt");
        //string rawText = File.ReadAllText(path);
        //FP.dialogue = DialogueManager.ParseFile(path);
        //Dialogue D = new Dialogue(path);
        //DialogueManager.MakeDialogue(D);
    }

    private IEnumerator Countdown()
    {
        float duration = 6f; // 3 seconds you can change this 
                             //to whatever you want
        float normalizedTime = 0;
        while (normalizedTime <= 1f)
        {
            countdownImage.fillAmount = 1f - normalizedTime;
            Debug.Log(duration-normalizedTime);
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }
    }

    // Update is called once per frame
    void Update()
    {
     

    }

    void NewDialogue(string nameFile)
    {
        //StartCompteur(10);
        string path = Path.Combine(directoryPath, nameFile);
        string rawText = File.ReadAllText(path);
        //CloseResponses();
        D = new Dialogue(rawText);
        //FP.dialogue = DialogueManager.ParseFile(rawText);
        //StartCoroutine(FP.Interact());
        //StartCoroutine(DialogueManager.MakeDialogue(D));
        StartCoroutine(DialogueManager.Instance.MakeDialogue(D));
        return ;
    }

    /*// Update is called once per frame
    void Update()
    {
        if (boolChrono)
        {
            if (chrono > 0)
            {
                chrono = chrono - Time.deltaTime;
                //Debug.Log(chrono);
                float fillAmount = chrono / 10;
                progressBar.fillAmount = fillAmount;
            }
            else
            {
                boolChrono = false;
            }
        }
       
    }

    //fonctions simulant un dialogue
    private IEnumerator LetPlayerChoose(Dialogue D)
    {
        Line line;
        int choice = -1;
        while (choice < 0)
        {
            choice = GetChoice();
            yield return null;
        }
       line = D.Choose(choice);
        //On affiche la r�ponse au joueur
       showDialogue(line);
        //On ferme les r�ponses
        CloseResponses();
    }
    private int GetChoice()
    {
        if (Input.GetKey("0"))
        {
            Debug.Log(0);
            return 0;
        }else if (Input.GetKey("1"))
        {
            Debug.Log(1);
            return 1;
        }
        else if (Input.GetKey("2"))
        {
            Debug.Log(2);
            return 2;
        }
        else if (Input.GetKey("3"))
        {
            Debug.Log(3);
            return 3;
        }
        else if (Input.GetKey("4"))
        {
            Debug.Log(4);
            return 4;
        }
        else 
        {
            return -1;
        }
    }
    private IEnumerator MakeDialogue(Dialogue D)
    {
        Line line;
        if (!D.IsFinished())
        {
            if (!D.HasMultiplePath())
            {
                //CloseResponses();
                line=D.Choose();
                showDialogue(line);
                yield return new WaitForSeconds(2);
                yield return MakeDialogue(D);
            }
            else
            {
                //D.PrintInfos();
                ShowResponses(D);
                yield return new WaitForSeconds(2);
                yield return LetPlayerChoose(D);
                yield return new WaitForSeconds(2);
                yield return MakeDialogue(D);
            }
        }
        //CloseDialogue(D);
    }

    private void showDialogue(Line line)
    {
        textDialogue.text = line.Text;
        nomPerso.text = line.CharacterId;
    }
    private void ShowResponses(Dialogue D)
    {
        string s = "";
        for (int i = 0; i < D.CurrentLines.Count; i++)
        {
            bullesResponses[i].SetActive(true);
            s += i.ToString() + " : " + D.CurrentLines[i].Text + " | ";
            options[i].text = i.ToString();
            responses[i].text = D.CurrentLines[i].Text;

        }
        Debug.Log("Choose : " + s);
    }

    private void CloseResponses()
    {
        for (int i = 0; i < bullesResponses.Length; i++)
        {
            bullesResponses[i].SetActive(false);
        }
    }

    private void CloseDialogue()
    {
        bulleDialogue.enabled = false;
        CloseResponses();
        return;
    }

    private List<Line> ExploreDialogue(List<Line> currentLines, List<Line> exploration)
    {
        if (currentLines.Count == 0)
        {
            return exploration;
        }
        else
        {
            Line LineExplored = currentLines[0];
            if (!exploration.Contains(LineExplored))
            {
                exploration.Add(LineExplored);
                currentLines.AddRange(LineExplored.NextLines);
            }
            currentLines.RemoveAt(0);
            return ExploreDialogue(currentLines, exploration);
        }
    }

    public void StartCompteur(float timeMax)
    {
        Debug.Log("Test ------ start cpt");
        boolChrono = true;
        chrono = timeMax;
        return;
        //StartCoroutine(Chronometre(timeMax));
    }

    /*private IEnumerator Chronometre(float timeMax)
    {
        //Debug.Log("chrono :");
        //Debug.Log(chrono);
        //Debug.Log("Test ------ start chrono routine");
        if (chrono > 0)
        {
            //yield return new WaitFor
                //WaitForSeconds(0.1);
            Debug.Log("Test ------ continue chrono routine");
            chrono --;
            Debug.Log("chrono :");
            Debug.Log(chrono);
            float fillAmount = chrono / timeMax;
            progressBar.fillAmount = fillAmount;
            yield return Chronometre(timeMax);
        }

        /*if (chrono > 0)
        {
            yield return new WaitForSeconds(1);
            chrono--;
            Debug.Log("Test ------ continue chrono routine");
            yield return Chronometre();
        }
    }*/
}
