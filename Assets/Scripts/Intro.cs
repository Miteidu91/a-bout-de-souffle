using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{
    [SerializeField] Transform bgToMove;
    [SerializeField] Camera mCamera;
    [SerializeField] Transform endOfCredits;
    private bool end = false;

    void Start()
    {
        
    }

    void Update()
    {
        bgToMove.Translate(new Vector3(0.0f, 80.0f * Time.deltaTime, 0.0f));
        Vector3 screenPoint = mCamera.WorldToViewportPoint(endOfCredits.transform.position);
        if (!end && screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 70 && screenPoint.y > 0 && screenPoint.y < 1)
        {
            end = true;
            StartCoroutine(EndOfCredits());
        }
    }
    private IEnumerator EndOfCredits()
    {
        yield return new WaitForSeconds(16.2f);
        SceneManager.LoadScene(2);
    }

}
