using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class StartDialogue : Action
{
    public string DialogueName;
    public override IEnumerator Activate()
    {
        Dialogue dialogue = DialogueManager.Instance.ParseFileCSV(DialogueName);
        CursorManager.Instance.CanInteract = false;
        yield return DialogueManager.Instance.MakeDialogue(dialogue);
    }
}
