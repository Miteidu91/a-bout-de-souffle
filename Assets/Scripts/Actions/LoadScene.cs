using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : Action
{
    public int SceneToLoad;
    public override IEnumerator Activate()
    {
        Debug.Log("Je load la scène");
        Debug.Log(SceneToLoad);
        SceneManager.LoadScene(SceneToLoad);
        yield return null;
    }
}
