using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockArea : Action
{
    public bool ToLock;
    public Collider lockedArea;
    public override IEnumerator Activate()
    {
        lockedArea.enabled = ToLock;
        yield return null;
    }
}
