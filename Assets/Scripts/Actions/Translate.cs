using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Translate : Action
{
    public Vector3 direction;
    public float time = 1f;
    public override IEnumerator Activate()
    {
        Vector3 start = gameObject.transform.position;
        Vector3 dest = start + direction;
        for(float i = time; i > 0; i -= Time.deltaTime)
        {
            transform.position = dest + (i/time) * (start - dest);
        }
        transform.position = dest;
        yield return null;
    }
}
