using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetItem : Action
{
    public GameObject toScreenshot;
    public Indice IndiceGiven;
    public Object ObjectGiven;
    public override IEnumerator Activate()
    {
        Sprite s = null;
        if (toScreenshot != null)
        {
            toScreenshot.SetActive(true);
            IconGenerator ic = IconGenerator.MakeIconGenerator(5);
            Texture2D ns = ic.TakeObjectSnapshot(toScreenshot);
            s = Sprite.Create(ns, new Rect(0.0f, 0.0f, ns.width, ns.height), new Vector2(0.5f, 0.5f), 100.0f);
            toScreenshot.SetActive(false);
        }
        if (IndiceGiven != null && IndiceGiven.itemID != 0)
        {
            IndiceGiven.illustration = s;
            Debug.Log("Giving " + IndiceGiven.itemName);
            Inventory.Instance.AddItem(IndiceGiven);
        }
        if (ObjectGiven != null && ObjectGiven.itemID != 0)
        {
            ObjectGiven.illustration = s;
            Debug.Log("Giving " + ObjectGiven.itemName);
            Inventory.Instance.AddItem(ObjectGiven);
        }
        yield return null;
    }
}
