D_AC_IDLE
	CharId:Anna Lucia Cortez
	Spoken:False
	Silent:False
	End:False
	NextLines:D_DR_AC_END,D_DR_AC_QTC_1
	Conditions:
	Text:Qu'est ce que tu veux Dick ?

D_DR_AC_END
	CharId:Dick Reckard
	Spoken:False
	Silent:False
	End:False
	NextLines:D_AC_IDLE_END
	Conditions:
	Text:Bonne journée...

D_AC_IDLE_END
	CharId:Anna Lucia Cortez
	Spoken:False
	Silent:False
	End:True
	NextLines:D_AC_IDLE
	Conditions:
	Text:Ciao !



D_DR_AC_QTC_1
	CharId:Dick Reckard
	Spoken:False
	Silent:False
	End:False
	NextLines:D_AC_QTC_1
	Conditions:
	Text:Avez vous vu quoi que ce soit de louche hier soir ?

D_AC_QTC_1
	CharId:Anna Lucia Cortez
	Spoken:False
	Silent:False
	End:False
	NextLines:D_AC_QTC_2
	Conditions:
	Text:Non juste la routine, j'ai juste eu à faire à un homme ivre sur la voie publique.

D_AC_QTC_2
	CharId:Anna Lucia Cortez
	Spoken:False
	Silent:False
	End:False
	NextLines:D_DR_AC_QTC_2,D_DR_AC_END
	Conditions:
	Text:Il criait des inepties à propos de message sur les murs et d'organisation secrète.

D_DR_AC_QTC_2
	CharId:Dick Reckard
	Spoken:False
	Silent:False
	End:False
	NextLines:D_AC_QTC_3
	Conditions:
	Text:Par où est il parti ?


D_AC_QTC_3
	CharId:Anna Lucia Cortez
	Spoken:False
	Silent:False
	End:False
	NextLines:D_DR_AC_END
	Conditions:
	Text:Il est parti en direction du Blue Note.